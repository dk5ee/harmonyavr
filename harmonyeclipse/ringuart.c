/*
 * ringuart.c
 */
#include "ringuart.h"
#include "defines.h"
//bitsize should be between 2 and 7
#define RINGUART_RX_BUFFER_BITSIZE 7
#define RINGUART_TX_BUFFER_BITSIZE 7
#define RXRING_BUFFER (1<<RINGUART_RX_BUFFER_BITSIZE)
#define RXRING_WRAP (RXRING_BUFFER -1 )
#define TXRING_BUFFER (1<<RINGUART_TX_BUFFER_BITSIZE)
#define TXRING_WRAP (TXRING_BUFFER -1 )

#if defined (UDR)
#define myU2X U2X
#define myUBRRL UBRRL
#define myUBRRH UBRRH
#define myUCSRA UCSRA
#define myUCSRB UCSRB
#define myUCSRC UCSRC
#define myUCSZ0 UCSZ0
#define myUCSZ1 UCSZ1
#define myUDR UDR
#define myUDRIE UDRIE
#define myRXCIE RXCIE
#define myRXEN RXEN
#define myTXCIE TXCIE
#define myTXEN TXEN
#endif
#if defined (UDR0)
#define myU2X U2X0
#define myUBRRL UBRR0L
#define myUBRRH UBRR0H
#define myUCSRA UCSR0A
#define myUCSRB UCSR0B
#define myUCSRC UCSR0C
#define myUCSZ0 UCSZ00
#define myUCSZ1 UCSZ01
#define myUDR UDR0
#define myUDRIE UDRIE0
#define myRXCIE RXCIE0
#define myRXEN RXEN0
#define myTXCIE TXCIE0
#define myTXEN TXEN0
#endif
/*
 // for second uart comment block above, use this block instead:
 #if defined (UDR1)
 #define myU2X U2X1
 #define myUBRRL UBRR1L
 #define myUBRRH UBRR1H
 #define myUCSRA UCSR1A
 #define myUCSRB UCSR1B
 #define myUCSRC UCSR1C
 #define myUCSZ0 UCSZ10
 #define myUCSZ1 UCSZ11
 #define myUDR UDR1
 #define myUDRIE UDRIE1
 #define myRXCIE RXCIE1
 #define myRXEN RXEN1
 #define myTXCIE TXCIE1
 #define myTXEN TXEN1
 #endif
 */

static uint8_t RxRing[RXRING_BUFFER];
static uint8_t TxRing[TXRING_BUFFER];
static volatile uint8_t RxHead = 0;
static volatile uint8_t RxTail = 0;
static volatile uint8_t RxStatus = 0;
static volatile uint8_t TxHead = 0;
static volatile uint8_t TxTail = 0;

static void uart_31250(void) {
#if defined BAUD
#undef BAUD
#endif
#define BAUD 31250
#include <util/setbaud.h>
	myUBRRL = UBRRL_VALUE;
	myUBRRH = UBRRH_VALUE;
	myUCSRB = ((1 << myRXCIE) | (1 << myRXEN) | (1 << myTXEN));
	myUCSRC = (1 << myUCSZ1) | (1 << myUCSZ0);
#if USE_2X
	myUCSRA |= (1 << myU2X);
#else
	myUCSRA &= ~(1 << myU2X);
#endif
}

void ringuartinit() {
	cli();
	uart_31250();
	sei();
}
void txring_enqueue(uint8_t data) {
	//wait while txring is full..
	while ((((TxTail + 1) - TxHead) & TXRING_WRAP) == 0) {
		//do nothing
		// TODO: idle function?
	}
	uint8_t tempadr = (TxTail + 1) & TXRING_WRAP;
	TxRing[tempadr] = data;
	TxTail = tempadr;
	//now: always activate interrupt for sending
	myUCSRB |= (1 << myUDRIE);
}
uint8_t txring_count() {
	return ((TxTail - TxHead) & TXRING_WRAP);
}
uint8_t txring_space() {
	return (TXRING_WRAP - ((TxTail - TxHead) & TXRING_WRAP));
}
uint8_t rxring_count() {
	return ((RxTail - RxHead) & RXRING_WRAP);
}
uint8_t rxring_space() {
	return (RXRING_WRAP - ((RxTail - RxHead) & RXRING_WRAP));
}
uint8_t rxring_dequeue() {
	if (RxTail == RxHead) {
		return 255;
	} else {
		uint8_t tempadr = (RxHead + 1) & RXRING_WRAP;
		RxHead = tempadr;
		return RxRing[tempadr];
	}
}
uint8_t rxring_peek(uint8_t offset) {
	return RxRing[(1 + offset + RxHead) & RXRING_WRAP];
}

/*void ring_outbyte(uint8_t data) {
 txring_enqueue('0');
 txring_enqueue('x');
 txring_enqueue(hex[(data >> 4)]);
 txring_enqueue(hex[data & 15]);
 }*/
inline void ring_outchar(uint8_t data) {
	//wrapper
	txring_enqueue(data);
}

ISR ( USART0_RX_vect ) {
	LED2on
	uint8_t data;
	data = myUDR;
	uint8_t temp;
	temp = ((RxTail + 1) & RXRING_WRAP);
	if (temp == RxHead) {
		RxStatus |= 4; //overflow happened.. OMG!
	} else {
		RxTail = temp;
		RxRing[temp] = data;
	}
	LED2off
}
ISR(USART0_UDRE_vect) {
	if ((TxTail - TxHead) & TXRING_WRAP) {
		uint8_t temp;
		temp = ((TxHead + 1) & TXRING_WRAP);
		TxHead = temp;
		myUDR = TxRing[temp];
	} else {
		myUCSRB &= ~(1 << myUDRIE); //disable sending interrupt
	}
}

// unused
EMPTY_INTERRUPT(USART0_TX_vect);
EMPTY_INTERRUPT(USART1_UDRE_vect);
EMPTY_INTERRUPT(USART1_RX_vect);
EMPTY_INTERRUPT(USART1_TX_vect);

