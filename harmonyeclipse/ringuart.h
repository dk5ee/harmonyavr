/*
 * ringuart.h
 *
 */

#ifndef RINGUART_H_
#define RINGUART_H_
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/twi.h>
#include <avr/pgmspace.h>

void ringuartinit();
void txring_enqueue(uint8_t data); // - am ende einfügen. automatisch senden starten.
uint8_t txring_count(); // - noch zu sendende zeichen im buffer
uint8_t txring_space(); // - noch verfügbarer space im buffer
uint8_t rxring_count(); // - empfangene zeichen im buffer
uint8_t rxring_space(); // - noch verfügbarer space im buffer
uint8_t rxring_dequeue(); // - erstes zeichen entfernen und ausliefern
uint8_t rxring_peek(uint8_t offset); // - kommendes zeichen entfernen
uint8_t rxstatus();
uint8_t rxresetstatus();

#endif /* RINGUART_H_ */
