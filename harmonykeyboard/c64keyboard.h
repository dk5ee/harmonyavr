/*
 * c64keyboard.h
 *
 */

#ifndef C64KEYBOARD_H_
#define C64KEYBOARD_H_

#include <avr/io.h>

void init64ports();


uint8_t getkey(uint8_t key);
uint8_t getoldkey(uint8_t key);
void scankeys();


#endif /* C64KEYBOARD_H_ */
