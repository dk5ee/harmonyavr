/*
 * sidfunctions.c
 *
 */
#include "sidfunctions.h"
#include <avr/pgmspace.h>
#include "defines.h"
const char text2[] PROGMEM = "end of midi2sidfreq";
const uint16_t midinote2sid[] PROGMEM = { 137, // Midi Note 0, 8.175799Hz
		137, // Midi Note C  0, 8.175799Hz
		145, // Midi Note C# 1, 8.661957Hz
		154, // Midi Note D  2, 9.177024Hz
		163, // Midi Note D# 3, 9.722718Hz
		173, // Midi Note E  4, 10.300861Hz
		183, // Midi Note F  5, 10.913382Hz
		194, // Midi Note F# 6, 11.562326Hz
		206, // Midi Note G  7, 12.249857Hz
		218, // Midi Note G# 8, 12.978272Hz
		231, // Midi Note A  9, 13.750000Hz
		244, // Midi Note A# 10, 14.567618Hz
		259, // Midi Note H  11, 15.433853Hz
		274, // Midi Note C  12, 16.351598Hz
		291, // Midi Note C# 13, 17.323914Hz
		308, // Midi Note D  14, 18.354048Hz
		326, // Midi Note D# 15, 19.445436Hz
		346, // Midi Note E  16, 20.601722Hz
		366, // Midi Note F  17, 21.826764Hz
		388, // Midi Note F# 18, 23.124651Hz
		411, // Midi Note G  19, 24.499715Hz
		435, // Midi Note G# 20, 25.956544Hz
		461, // Midi Note A  21, 27.500000Hz
		489, // Midi Note A# 22, 29.135235Hz
		518, // Midi Note H  23, 30.867706Hz
		549, // Midi Note C  24, 32.703196Hz
		581, // Midi Note C# 25, 34.647829Hz
		616, // Midi Note D  26, 36.708096Hz
		652, // Midi Note D# 27, 38.890873Hz
		691, // Midi Note E  28, 41.203445Hz
		732, // Midi Note F  29, 43.653529Hz
		776, // Midi Note F# 30, 46.249303Hz
		822, // Midi Note G  31, 48.999429Hz
		871, // Midi Note G# 32, 51.913087Hz
		923, // Midi Note A  33, 55.000000Hz
		978, // Midi Note A# 34, 58.270470Hz
		1036, // Midi Note H  35, 61.735413Hz
		1097, // Midi Note C  36, 65.406391Hz
		1163, // Midi Note C# 37, 69.295658Hz
		1232, // Midi Note D  38, 73.416192Hz
		1305, // Midi Note D# 39, 77.781746Hz
		1383, // Midi Note E  40, 82.406889Hz
		1465, // Midi Note F  41, 87.307058Hz
		1552, // Midi Note F# 42, 92.498606Hz
		1644, // Midi Note G  43, 97.998859Hz
		1742, // Midi Note G# 44, 103.826174Hz
		1845, // Midi Note A  45, 110.000000Hz
		1955, // Midi Note A# 46, 116.540940Hz
		2071, // Midi Note H  47, 123.470825Hz
		2195, // Midi Note C  48, 130.812783Hz
		2325, // Midi Note C# 49, 138.591315Hz
		2463, // Midi Note D  50, 146.832384Hz
		2610, // Midi Note D# 51, 155.563492Hz
		2765, // Midi Note E  52, 164.813778Hz
		2930, // Midi Note F  53, 174.614116Hz
		3104, // Midi Note F# 54, 184.997211Hz
		3288, // Midi Note G  55, 195.997718Hz
		3484, // Midi Note G# 56, 207.652349Hz
		3691, // Midi Note A  57, 220.000000Hz
		3910, // Midi Note A# 58, 233.081881Hz
		4143, // Midi Note H  59, 246.941651Hz
		4389, // Midi Note C  60, 261.625565Hz
		4650, // Midi Note C# 61, 277.182631Hz
		4927, // Midi Note D  62, 293.664768Hz
		5220, // Midi Note D# 63, 311.126984Hz
		5530, // Midi Note E  64, 329.627557Hz
		5859, // Midi Note F  65, 349.228231Hz
		6207, // Midi Note F# 66, 369.994423Hz
		6577, // Midi Note G  67, 391.995436Hz
		6968, // Midi Note G# 68, 415.304698Hz
		7382, // Midi Note A  69, 440.000000Hz
		7821, // Midi Note A# 70, 466.163762Hz
		8286, // Midi Note H  71, 493.883301Hz
		8779, // Midi Note C  72, 523.251131Hz
		9301, // Midi Note C# 73, 554.365262Hz
		9854, // Midi Note D  74, 587.329536Hz
		10440, // Midi Note D# 75, 622.253967Hz
		11060, // Midi Note E  76, 659.255114Hz
		11718, // Midi Note F  77, 698.456463Hz
		12415, // Midi Note F# 78, 739.988845Hz
		13153, // Midi Note G  79, 783.990872Hz
		13935, // Midi Note G# 80, 830.609395Hz
		14764, // Midi Note A  81, 880.000000Hz
		15642, // Midi Note A# 82, 932.327523Hz
		16572, // Midi Note H  83, 987.766603Hz
		17557, // Midi Note C  84, 1046.502261Hz
		18601, // Midi Note C# 85, 1108.730524Hz
		19708, // Midi Note D  86, 1174.659072Hz
		20879, // Midi Note D# 87, 1244.507935Hz
		22121, // Midi Note E  88, 1318.510228Hz
		23436, // Midi Note F  89, 1396.912926Hz
		24830, // Midi Note F# 90, 1479.977691Hz
		26306, // Midi Note G  91, 1567.981744Hz
		27871, // Midi Note G# 92, 1661.218790Hz
		29528, // Midi Note A  93, 1760.000000Hz
		31284, // Midi Note A# 94, 1864.655046Hz
		33144, // Midi Note H  95, 1975.533205Hz
		35115, // Midi Note C  96, 2093.004522Hz
		37203, // Midi Note C# 97, 2217.461048Hz
		39415, // Midi Note D  98, 2349.318143Hz
		41759, // Midi Note D# 99, 2489.015870Hz
		44242, // Midi Note E  100, 2637.020455Hz
		46873, // Midi Note F  101, 2793.825851Hz
		49660, // Midi Note F# 102, 2959.955382Hz
		52613, // Midi Note G  103, 3135.963488Hz
		55741, // Midi Note G# 104, 3322.437581Hz
		59056, // Midi Note A  105, 3520.000000Hz
		62567, // Midi Note A# 106, 3729.310092Hz
		};
const uint16_t midilastnote = 106;
uint16_t sidgetfreq(uint8_t midinote) {
	return pgm_read_word(&(midinote2sid[midinote]));
}

uint8_t sidshadowreg[25];
//todo: if read out from sid is implementated, increase from 24 to 28

uint8_t sidshadowctl[3];
#define SIDSHADOWCTLNOEND (1<<0)
#define SIDSHADOWCTLDRUM (1<<1)
uint8_t sidshadownotelen[3];

//supposed to be called every 20 ms
void sidframetick() {
	uint8_t chan = 0;
	for (chan = 0; chan < 3; chan++) {
		if (sidshadownotelen[chan] > 0) {
			sidshadownotelen[chan]--;
			if ((sidshadownotelen[chan] == 0)
					&& ((sidshadowctl[chan] & SIDSHADOWCTLNOEND) == 0)) {
				//end channel
			}
		}
	}
	//perform fx here..
}

uint8_t sidreadreg(uint8_t reg) {
	return sidshadowreg[reg];
}

void sidwritereg(uint8_t reg, uint8_t value) {
	if (reg < 25) {
		sidshadowreg[reg] = value;
		//tbi
		//first set adress line and data lines
		SIDAdress = reg;
		SIDData = value;
		//pull write to low; if no reading, rw can stay low
		SIDUnsetRW
		//pull cs to low
		SIDUnsetCS
		//wait 20 20mhz cycles for a whole 1mhz cycle
		asm volatile (
				"nop" "\n\t" //1
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"//20
		);
		//pull cs to high
		SIDSetCS
		//wait half a cycle to be sure
		asm volatile (
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
				"nop" "\n\t"
		);
	}
}

void sidwritemaskedreg(uint8_t reg, uint8_t value, uint8_t mask) {
	uint8_t oldvalue = sidshadowreg[reg] & ~(mask);
	sidwritereg(reg, oldvalue | value);
}

void sidsetnote(uint8_t channel, uint8_t note) {
	if (note <= midilastnote) {
		sidsetfreq(channel, sidgetfreq(note));
	}
}
/*
 channel 0..2 normal voices
 channel 3: frequency cutoff
 */
void sidsetfreq(uint8_t channel, uint16_t value) {
	uint8_t offset = channel & 0x03;
	uint8_t lowval, highval;
	if (channel == 0x03) {
		lowval = value & 0x07;
		highval = value >> 5;
	} else {
		lowval = value & 0xff;
		highval = value >> 8;
	}
	sidwritereg(offset * 7, lowval);
	sidwritereg(offset * 7 + 1, highval);
}

void sidsetADSR(uint8_t channel, uint8_t AD, uint8_t SR) {
	if (channel < 3) {
		sidwritereg(channel * 7 + 5, AD);
		sidwritereg(channel * 7 + 6, SR);
	}
}
void sidsetcontrol(uint8_t channel, uint8_t control) {
	if (channel < 3) {
		sidwritereg(channel * 7 + 4, control);
	}
}
void sidsetfilterfreq(uint16_t filter) {
	uint8_t lowval = filter & 0x15;
	uint8_t highval = filter >> 4;
	sidwritereg(21, lowval);
	sidwritereg(22, highval);
}
void sidsetRESFilt(uint8_t resfilt) {
	sidwritereg(23, resfilt);
}
void sidsetmode(uint8_t mode) {
	sidwritereg(24, mode);
}
void sidsetwaveform(uint8_t channel, uint8_t waveform) {
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 4, waveform << 4, 0xf0);
	}
}
void sidsetgate(uint8_t channel, uint8_t gate) {
	if (gate) {
		gate = 1;
	}
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 4, gate, 0x01);
	}
}
void sidsetring(uint8_t channel, uint8_t value) {
	if (value) {
		value = 1 << 2;
	}
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 4, value, 1 << 2);
	}
}
void sidsetsync(uint8_t channel, uint8_t value) {
	if (value) {
		value = 1 << 1;
	}
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 4, value, 1 << 1);
	}
}
void sidsettest(uint8_t channel, uint8_t value) {
	if (value) {
		value = 1 << 3;
	}
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 4, value, 1 << 3);
	}
}
void sidsetA(uint8_t channel, uint8_t value) {
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 5, value << 4, 0xf0);
	}
}
void sidsetD(uint8_t channel, uint8_t value) {
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 5, value & 0x0f, 0x0f);
	}
}
void sidsetS(uint8_t channel, uint8_t value) {
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 6, value << 4, 0xf0);
	}
}
void sidsetR(uint8_t channel, uint8_t value) {
	if (channel < 3) {
		sidwritemaskedreg(channel * 7 + 6, value & 0x0f, 0x0f);
	}
}
void sidsetres(uint8_t value) {
	sidwritemaskedreg(23, value << 4, 0xf0);
}
void sidsetfilter(uint8_t value) {
	sidwritemaskedreg(23, value & 0x0f, 0x0f);
}
void sidsetvolume(uint8_t value) {
	sidwritemaskedreg(24, value & 0x0f, 0x0f);
}
void sidsetfiltertype(uint8_t value) {
	sidwritemaskedreg(24, (value & 0x03) << 4, 0x70);
}
void sidsetdisable3(uint8_t value) {
	if (value) {
		value = 1 << 7;
	}
	sidwritemaskedreg(24, value, 1 << 7);
}

void sidloadinstrument(uint8_t channel, uint8_t instrument) {
	if (channel < 3) {
		uint8_t PWM1;
		uint8_t PWM2;
		uint8_t CTL;
		uint8_t AD;
		uint8_t SR;
		uint8_t VIB;
		uint8_t PS;
		uint8_t FX;
		switch (instrument) {
		//robbed hubbard instruments
		case 0:
			PWM1 = 0x80;
			PWM2 = 0x09;
			CTL = 0x41;
			AD = 0x48;
			SR = 0x60;
			VIB = 0x03;
			PS = 0x81;
			FX = 0x00;
			break;
		case 1:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x81;
			AD = 0x02;
			SR = 0x08;
			VIB = 0x00;
			PS = 0x00;
			FX = 0x01;
			break;
		case 2:
			PWM1 = 0xa0;
			PWM2 = 0x02;
			CTL = 0x41;
			AD = 0x09;
			SR = 0x80;
			VIB = 0x00;
			PS = 0x00;
			FX = 0x00;
			break;
		case 3:
			PWM1 = 0x00;
			PWM2 = 0x02;
			CTL = 0x81;
			AD = 0x09;
			SR = 0x09;
			VIB = 0x00;
			PS = 0x00;
			FX = 0x05;
			break;
		case 4:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x41;
			AD = 0x08;
			SR = 0x50;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x04;
			break;
		case 5:
			PWM1 = 0x00;
			PWM2 = 0x01;
			CTL = 0x41;
			AD = 0x3f;
			SR = 0xc0;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		case 6:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x41;
			AD = 0x04;
			SR = 0x40;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		case 7:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x41;
			AD = 0x09;
			SR = 0x00;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		case 8:
			PWM1 = 0x00;
			PWM2 = 0x09;
			CTL = 0x41;
			AD = 0x09;
			SR = 0x70;
			VIB = 0x02;
			PS = 0x5f;
			FX = 0x04;
			break;
		case 9:
			PWM1 = 0x00;
			PWM2 = 0x09;
			CTL = 0x41;
			AD = 0x4a;
			SR = 0x69;
			VIB = 0x02;
			PS = 0x81;
			FX = 0x00;
			break;
		case 10:
			PWM1 = 0x00;
			PWM2 = 0x09;
			CTL = 0x41;
			AD = 0x40;
			SR = 0x6f;
			VIB = 0x00;
			PS = 0x81;
			FX = 0x02;
			break;
		case 11:
			PWM1 = 0x80;
			PWM2 = 0x07;
			CTL = 0x81;
			AD = 0x0a;
			SR = 0x0a;
			VIB = 0x00;
			PS = 0x00;
			FX = 0x01;
			break;
		case 12:
			PWM1 = 0x00;
			PWM2 = 0x09;
			CTL = 0x41;
			AD = 0x3f;
			SR = 0xff;
			VIB = 0x01;
			PS = 0xe7;
			FX = 0x02;
			break;
		case 13:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x41;
			AD = 0x90;
			SR = 0xf0;
			VIB = 0x01;
			PS = 0xe8;
			FX = 0x02;
			break;
		case 14:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x41;
			AD = 0x06;
			SR = 0x0a;
			VIB = 0x00;
			PS = 0x00;
			FX = 0x01;
			break;
		case 15:
			PWM1 = 0x00;
			PWM2 = 0x09;
			CTL = 0x41;
			AD = 0x19;
			SR = 0x70;
			VIB = 0x02;
			PS = 0xa8;
			FX = 0x00;
			break;
		case 16:
			PWM1 = 0x00;
			PWM2 = 0x02;
			CTL = 0x41;
			AD = 0x09;
			SR = 0x90;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		case 17:
			PWM1 = 0x00;
			PWM2 = 0x00;
			CTL = 0x11;
			AD = 0x0a;
			SR = 0xfa;
			VIB = 0x00;
			PS = 0x00;
			FX = 0x05;
			break;
		case 18:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x41;
			AD = 0x37;
			SR = 0x40;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		case 19:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x11;
			AD = 0x07;
			SR = 0x70;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		default:
			PWM1 = 0x00;
			PWM2 = 0x08;
			CTL = 0x11;
			AD = 0x07;
			SR = 0x70;
			VIB = 0x02;
			PS = 0x00;
			FX = 0x00;
			break;
		}

		sidwritereg(channel * 7 + 2, PWM1);
		sidwritereg(channel * 7 + 3, PWM2);
		sidwritereg(channel * 7 + 4, CTL & 0xFE);
		sidwritereg(channel * 7 + 5, AD);
		sidwritereg(channel * 7 + 6, SR);
//todo: just to fix compiler warnings now..
		sidshadowctl[channel] = VIB & PS & FX & CTL;
	}
}

void sidinit() {
	SIDInit;
	SIDSetCS;
	SIDSetRW;
	SIDSetReset;
	SIDStartOsc;
	sidloadinstrument(0,0);
	sidloadinstrument(1,1);
	sidloadinstrument(2,2);
}
