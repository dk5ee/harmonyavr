/*
 * cpu.c
 *
 *
 */
#include "memory.h"
#include "cpu.h"
#define FN 0x80
#define FV 0x40
#define FB 0x10
#define FD 0x08
#define FI 0x04
#define FZ 0x02
#define FC 0x01

//typedef for opcodes
typedef void (*fun6502_t)(void);


uint16_t pc_6502;
uint8_t a_6502;
uint8_t x_6502;
uint8_t y_6502;
uint8_t flags_6502;
uint8_t sp_6502;
uint8_t broken_6502 = 0; //instead of return value
inline void internal6502_writebyte(uint16_t address, uint8_t data) {
	writefakememory(address, data);
}

inline uint8_t internal6502_readbyte(uint16_t address) {
	return readfakememory(address);

}

uint16_t internal6502_readword(uint16_t address) {
	return readfakememory(address) + 256 * readfakememory(address + 1);
}

uint16_t internal6502_memadd_indexed_indirect() {
	uint8_t address = internal6502_readbyte(pc_6502) + x_6502;
	pc_6502++;
	return internal6502_readword(address);
}

uint16_t internal6502_memadd_indirect_indexed() {
	uint8_t address = internal6502_readbyte(pc_6502);
	pc_6502++;
	return (internal6502_readword(address) + y_6502);
}

void internal6502_acuflags() {
	flags_6502 &= ~(FN | FZ);
	if (a_6502 > 127) {
		flags_6502 |= FN;
	}
	if (a_6502 == 0) {
		flags_6502 |= FZ;
	}
}

void internal6502_valueflags(uint8_t v) {
	flags_6502 &= ~(FN | FZ);
	if (v > 127) {
		flags_6502 |= FN;
	}
	if (v == 0) {
		flags_6502 |= FZ;
	}
}

void internal6502_pushbyte(uint8_t data) {
	c64stack_6502[sp_6502] = data;
	sp_6502--;
}

void internal6502_pushword(uint16_t data) {
	//todo: endianess correct?
	//here:
	//http://wilsonminesco.com/stacks/sub_ret.html
	//first the MS byte is on top of stack..

	internal6502_pushbyte(data >> 8);
	internal6502_pushbyte(data & 0xff);
}

uint8_t internal6502_popbyte() {
	sp_6502++;
	return c64stack_6502[sp_6502];
}

uint16_t internal6502_popword() {
	//todo: endianess correct?
	//see internal6502_pushword()
	uint8_t data1 = internal6502_popbyte();
	uint8_t data2 = internal6502_popbyte();
	return data1 + (data2 << 8);
}

void internal6502_branch() {
	uint8_t data = internal6502_readbyte(pc_6502 + 1);
	if (data > 127) {
		pc_6502 = pc_6502 - 256;
	}
	pc_6502 = pc_6502 + data;
}

uint8_t internal6502_immediate() {
	uint8_t data = internal6502_readbyte(pc_6502);
	pc_6502++;
	return data;
}

uint16_t internal6502_absolute_address() {
	uint8_t data1 = internal6502_readbyte(pc_6502);
	pc_6502++;
	//todo: does page crossing affect here?
	uint8_t data2 = internal6502_readbyte(pc_6502);
	pc_6502++;
	return (data2 << 8 | data1);
}

/*
 uint8_t TEMPLATE_6502(uint8_t a) {
 return 0;
 }
 */
void BRK_6502x00() {
	internal6502_pushword(pc_6502);
	pc_6502 = internal6502_readword(0xfffe);
	broken_6502 = 3;
}

uint8_t addhelper6502(uint8_t a, uint8_t b) {
	//bcd.. code - me was lazy..
	if (flags_6502 & FD) {
		uint8_t blow = (a & 0x0f) + (b & 0x0f);
		uint8_t bhigh = (a & 0xf0) + (b & 0xf0);
		if (blow >= 10) {
			blow -= 10;
			bhigh += 16;
		}
		bhigh = bhigh + blow;
		if (bhigh >= 160) {
			bhigh -= 160;
			flags_6502 |= FV;
		} else {
			flags_6502 &= ~FV;
		}
		return bhigh;
	} else {
		uint16_t r = a + b;
		if (r >= 256) {
			flags_6502 |= FV;
		} else {
			flags_6502 &= ~FV;
		}
		if (r >= 128) {
			flags_6502 |= FN;
		} else {
			flags_6502 &= ~FN;
		}
		return a + b;
	}
}

void ADC_6502x61() {
	//(Ind,X)
	uint8_t d = internal6502_readbyte(internal6502_memadd_indexed_indirect());
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x65() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	uint8_t d = c64zeropage_6502[data];
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x69() {
	//(Immediate)
	uint8_t d = internal6502_immediate();
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x6D() {
	//(Absolute)
	uint8_t d = internal6502_absolute_address();
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x71() {
	//((Ind),Y)
	uint8_t d = internal6502_readbyte(internal6502_memadd_indirect_indexed());
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x75() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	uint8_t d = c64zeropage_6502[data];
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x79() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	uint8_t d = internal6502_readbyte(address);
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

void ADC_6502x7D() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t d = internal6502_readbyte(address);
	a_6502 = addhelper6502(a_6502, d);
	internal6502_acuflags();

}

uint8_t subhelper6502(uint8_t a, uint8_t b) {
	//bcd.. code - me was lazy..
	if (flags_6502 & FD) {
		uint8_t blow = (a & 0x0f) - (b & 0x0f);
		uint8_t bhigh = (a & 0xf0) - (b & 0xf0);
		if (blow >= 10) {
			blow += 10;
			bhigh -= 16;
		}
		bhigh = bhigh + blow;
		if (bhigh >= 160) {
			bhigh += 160;
			flags_6502 |= FV;
		} else {
			flags_6502 &= ~FV;
		}
		return bhigh;
	} else {
		uint16_t r = a - b;
		if (r >= 256) {
			flags_6502 |= FV;
		} else {
			flags_6502 &= ~FV;
		}
		if (r >= 128) {
			flags_6502 |= FN;
		} else {
			flags_6502 &= ~FN;
		}
		return a + b;
	}
}

void SBC_6502xE1() {
	//(Ind,X)
	uint8_t d = internal6502_readbyte(internal6502_memadd_indexed_indirect());
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xE5() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	uint8_t d = c64zeropage_6502[data];
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xE9() {
	//(Immediate)
	uint8_t d = internal6502_immediate();
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xED() {
	//(Absolute)
	uint8_t d = internal6502_absolute_address();
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xF1() {
	//((Ind),Y)
	uint8_t d = internal6502_readbyte(internal6502_memadd_indirect_indexed());
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xF5() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	uint8_t d = c64zeropage_6502[data];
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xF9() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	uint8_t d = internal6502_readbyte(address);
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void SBC_6502xFD() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t d = internal6502_readbyte(address);
	a_6502 = subhelper6502(a_6502, d);
	internal6502_acuflags();

}

void STA_6502x81() {
	//(Ind,X)
	internal6502_writebyte(internal6502_memadd_indexed_indirect(), a_6502);

}

void STA_6502x85() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	c64zeropage_6502[data] = a_6502;

}

void STA_6502x8D() {
	//(Absolute)
	internal6502_writebyte(internal6502_absolute_address(), a_6502);

}

void STA_6502x91() {
	//((Ind),Y)
	internal6502_writebyte(internal6502_memadd_indirect_indexed(), a_6502);

}

void STA_6502x95() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	c64zeropage_6502[data] = a_6502;

}

void STA_6502x99() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	internal6502_writebyte(address, a_6502);

}

void STA_6502x9D() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	internal6502_writebyte(address, a_6502);

}

void STX_6502x86() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	c64zeropage_6502[data] = x_6502;

}

void STX_6502x8E() {
	//(Absolute)
	internal6502_writebyte(internal6502_absolute_address(), x_6502);

}

void STX_6502x96() {
	//(Z-Page,Y)
	uint8_t data = internal6502_immediate();
	data += y_6502;
	c64zeropage_6502[data] = x_6502;

}

void STY_6502x84() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	c64zeropage_6502[data] = y_6502;

}

void STY_6502x8C() {
	//(Absolute)
	internal6502_writebyte(internal6502_absolute_address(), y_6502);

}

void STY_6502x94() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	c64zeropage_6502[data] = y_6502;

}

void LDA_6502xA1() {
	//(Ind,X)
	a_6502 = internal6502_readbyte(internal6502_memadd_indexed_indirect());
	internal6502_acuflags();

}

void LDA_6502xA5() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	a_6502 = c64zeropage_6502[data];
	internal6502_acuflags();

}

void LDA_6502xA9() {
	//(Immediate)
	a_6502 = internal6502_immediate();
	internal6502_acuflags();

}

void LDA_6502xAD() {
	//(Absolute)
	a_6502 = internal6502_absolute_address();
	internal6502_acuflags();

}

void LDA_6502xB1() {
	//((Ind),Y)
	a_6502 = internal6502_readbyte(internal6502_memadd_indirect_indexed());
	internal6502_acuflags();

}

void LDA_6502xB5() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	a_6502 = c64zeropage_6502[data];
	internal6502_acuflags();

}

void LDA_6502xB9() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	a_6502 = internal6502_readbyte(address);
	internal6502_acuflags();

}

void LDA_6502xBD() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	a_6502 = internal6502_readbyte(address);
	internal6502_acuflags();

}

void LDX_6502xA6() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	x_6502 = c64zeropage_6502[data];
	internal6502_valueflags(x_6502);

}

void LDX_6502xA2() {
	//(Immediate)
	x_6502 = internal6502_immediate();
	internal6502_valueflags(x_6502);

}

void LDX_6502xAE() {
	//(Absolute)
	x_6502 = internal6502_absolute_address();
	internal6502_valueflags(x_6502);

}

void LDX_6502xB6() {
	//(Z-Page,Y)
	uint8_t data = internal6502_immediate();
	data += y_6502;
	x_6502 = c64zeropage_6502[data];
	internal6502_valueflags(x_6502);

}

void LDX_6502xBE() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	x_6502 = internal6502_readbyte(address);
	internal6502_valueflags(x_6502);

}

void LDY_6502xA4() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	y_6502 = c64zeropage_6502[data];
	internal6502_valueflags(y_6502);

}

void LDY_6502xA0() {
	//(Immediate)
	y_6502 = internal6502_immediate();
	internal6502_valueflags(y_6502);

}

void LDY_6502xAC() {
	//(Absolute)
	y_6502 = internal6502_absolute_address();
	internal6502_valueflags(y_6502);

}

void LDY_6502xB4() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	y_6502 = c64zeropage_6502[data];
	internal6502_valueflags(y_6502);

}

void LDY_6502xBC() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	y_6502 = internal6502_readbyte(address);
	internal6502_valueflags(y_6502);

}

void CMP_6502xC1() {
	//(Ind,X)
	uint8_t data = internal6502_readbyte(
			internal6502_memadd_indexed_indirect());
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xC5() {
	//(Z-Page)
	uint8_t data2 = internal6502_immediate();
	uint8_t data = c64zeropage_6502[data2];
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xC9() {
	//(Immediate)
	uint8_t data = internal6502_immediate();
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xCD() {
	//(Absolute)
	uint8_t data = internal6502_absolute_address();
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xD1() {
	//((Ind),Y)
	uint8_t data = internal6502_readbyte(
			internal6502_memadd_indirect_indexed());
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xD5() {
	//(Z-Page,X)
	uint8_t data1 = internal6502_immediate();
	data1 += x_6502;
	uint8_t data = c64zeropage_6502[data1];
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xD9() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	uint8_t data = internal6502_readbyte(address);
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CMP_6502xDD() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t data = internal6502_readbyte(address);
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CPX_6502xE4() {
	//(Z-Page)
	uint8_t data = c64zeropage_6502[internal6502_immediate()];
	flags_6502 &= ~(FN | FZ | FC);
	if (x_6502 == data) {
		flags_6502 |= FZ;
	}
	if (x_6502 > 127) {
		flags_6502 |= FN;
	}
	if (x_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CPX_6502xE0() {
	//(Immediate)
	uint8_t data = internal6502_immediate();
	flags_6502 &= ~(FN | FZ | FC);
	if (x_6502 == data) {
		flags_6502 |= FZ;
	}
	if (x_6502 > 127) {
		flags_6502 |= FN;
	}
	if (x_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CPX_6502xEC() {
	//(Absolute)
	uint8_t data = internal6502_absolute_address();
	flags_6502 &= ~(FN | FZ | FC);
	if (x_6502 == data) {
		flags_6502 |= FZ;
	}
	if (x_6502 > 127) {
		flags_6502 |= FN;
	}
	if (x_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CPY_6502xC4() {
	//(Z-Page)
	uint8_t data = c64zeropage_6502[internal6502_immediate()];
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CPY_6502xC0() {
	//(Immediate)
	uint8_t data = internal6502_immediate();
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void CPY_6502xCC() {
	//(Absolute)
	uint8_t data = internal6502_absolute_address();
	flags_6502 &= ~(FN | FZ | FC);
	if (y_6502 == data) {
		flags_6502 |= FZ;
	}
	if (y_6502 > 127) {
		flags_6502 |= FN;
	}
	if (y_6502 >= data) {
		flags_6502 |= FC;
	}

}

void ORA_6502x01() {
	//(Ind,X)
	a_6502 = a_6502
			| internal6502_readbyte(internal6502_memadd_indexed_indirect());
	internal6502_acuflags();

}

void ORA_6502x05() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	a_6502 = a_6502 | c64zeropage_6502[data];
	internal6502_acuflags();

}

void ORA_6502x09() {
	//(Immediate)
	a_6502 = a_6502 | internal6502_immediate();
	internal6502_acuflags();

}

void ORA_6502x0D() {
	//(Absolute)
	a_6502 = a_6502 | internal6502_absolute_address();
	internal6502_acuflags();

}

void ORA_6502x11() {
	//((Ind),Y)
	a_6502 = a_6502
			| internal6502_readbyte(internal6502_memadd_indirect_indexed());
	internal6502_acuflags();

}

void ORA_6502x15() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	a_6502 = a_6502 | c64zeropage_6502[data];
	internal6502_acuflags();

}

void ORA_6502x19() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	a_6502 = a_6502 | internal6502_readbyte(address);
	internal6502_acuflags();

}

void ORA_6502x1D() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	a_6502 = a_6502 | internal6502_readbyte(address);
	internal6502_acuflags();

}

void EOR_6502x41() {
	//(Ind,X)
	a_6502 = a_6502
			^ internal6502_readbyte(internal6502_memadd_indexed_indirect());
	internal6502_acuflags();

}

void EOR_6502x45() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	a_6502 = a_6502 ^ c64zeropage_6502[data];
	internal6502_acuflags();

}

void EOR_6502x49() {
	//(Immediate)
	a_6502 = a_6502 ^ internal6502_immediate();
	internal6502_acuflags();

}

void EOR_6502x4D() {
	//(Absolute)
	a_6502 = a_6502 ^ internal6502_absolute_address();
	internal6502_acuflags();

}

void EOR_6502x51() {
	//((Ind),Y)
	a_6502 = a_6502
			^ internal6502_readbyte(internal6502_memadd_indirect_indexed());
	internal6502_acuflags();

}

void EOR_6502x55() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	a_6502 = a_6502 ^ c64zeropage_6502[data];
	internal6502_acuflags();

}

void EOR_6502x59() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	a_6502 = a_6502 ^ internal6502_readbyte(address);
	internal6502_acuflags();

}

void EOR_6502x5D() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	a_6502 = a_6502 ^ internal6502_readbyte(address);
	internal6502_acuflags();

}

void AND_6502x21() {
	//(Ind,X)
	a_6502 = a_6502
			& internal6502_readbyte(internal6502_memadd_indexed_indirect());
	internal6502_acuflags();

}

void AND_6502x25() {
	//(Z-Page)
	uint8_t data = internal6502_immediate();
	a_6502 = a_6502 & c64zeropage_6502[data];
	internal6502_acuflags();

}

void AND_6502x29() {
	//(Immediate)
	a_6502 = a_6502 & internal6502_immediate();
	internal6502_acuflags();

}

void AND_6502x2D() {
	//(Absolute)
	a_6502 = a_6502 & internal6502_absolute_address();
	internal6502_acuflags();

}

void AND_6502x31() {
	//((Ind),Y)
	a_6502 = a_6502
			& internal6502_readbyte(internal6502_memadd_indirect_indexed());
	internal6502_acuflags();

}

void AND_6502x35() {
	//(Z-Page,X)
	uint8_t data = internal6502_immediate();
	data += x_6502;
	a_6502 = a_6502 & c64zeropage_6502[data];
	internal6502_acuflags();

}

void AND_6502x39() {
	//(Absolute,Y)
	uint16_t address = internal6502_absolute_address() + y_6502;
	a_6502 = a_6502 & internal6502_readbyte(address);
	internal6502_acuflags();

}

void AND_6502x3D() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	a_6502 = a_6502 & internal6502_readbyte(address);
	internal6502_acuflags();

}

void BIT_6502x24() {
	//(Z-Page)
	//Z <- ~(A /\ M) N<-M7 V<-M6
	uint8_t data = internal6502_immediate();
	uint8_t value = a_6502 & c64zeropage_6502[data];
	flags_6502 &= ~(FN & FV);
	if (value & (1 << 7)) {
		flags_6502 |= FN;
	}
	if (value & (1 << 6)) {
		flags_6502 |= FV;
	}

}

void BIT_6502x2C() {
	//(Absolute)
	//Z <- ~(A /\ M) N<-M7 V<-M6
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t value = a_6502 & internal6502_readbyte(address);
	flags_6502 &= ~(FN & FV);
	if (value & (1 << 7)) {
		flags_6502 |= FN;
	}
	if (value & (1 << 6)) {
		flags_6502 |= FV;
	}

}

void LSR_6502x46() {
	//(Z-Page)
	uint8_t address = internal6502_immediate();
	uint8_t data = c64zeropage_6502[address];
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	c64zeropage_6502[address] = data;
	internal6502_valueflags(data);

}

void LSR_6502x4A() {
	//(Accumulator)
	flags_6502 &= ~FC;
	if (a_6502 & (1 << 7)) {
		flags_6502 |= FC;
	}
	a_6502 = a_6502 << 1;
	internal6502_acuflags();

}

void LSR_6502x56() {
	//(Z-Page,X)
	uint8_t address = internal6502_immediate();
	address += x_6502;
	uint8_t data = c64zeropage_6502[address];
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	c64zeropage_6502[address] = data;
	internal6502_valueflags(data);

}

void LSR_6502x4E() {
	//(Absolute)
	uint16_t address = internal6502_absolute_address();
	uint8_t data = internal6502_readbyte(address);
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void LSR_6502x5E() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t data = internal6502_readbyte(address);
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void ROR_6502x66() {
	//(Z-Page)
	uint8_t address = internal6502_immediate();
	uint8_t data = c64zeropage_6502[address];
	uint8_t carry = (flags_6502 & FC) ? 1 : 0;
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	data += carry;
	c64zeropage_6502[address] = data;
	internal6502_valueflags(data);

}

void ROR_6502x6A() {
	//(Accumulator)
	uint8_t carry = (flags_6502 & FC) ? 1 : 0;
	flags_6502 &= ~FC;
	if (a_6502 & (1 << 7)) {
		flags_6502 |= FC;
	}
	a_6502 = a_6502 << 1;
	a_6502 += carry;
	internal6502_acuflags();

}

void ROR_6502x76() {
	//(Z-Page,X)
	uint8_t address = internal6502_immediate();
	address += x_6502;
	uint8_t data = c64zeropage_6502[address];
	uint8_t carry = (flags_6502 & FC) ? 1 : 0;
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	data += carry;
	c64zeropage_6502[address] = data;
	internal6502_valueflags(data);

}

void ROR_6502x6E() {
	//(Absolute)
	uint16_t address = internal6502_absolute_address();
	uint8_t data = internal6502_readbyte(address);
	uint8_t carry = (flags_6502 & FC) ? 1 : 0;
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	data += carry;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void ROR_6502x7E() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t data = internal6502_readbyte(address);
	uint8_t carry = (flags_6502 & FC) ? 1 : 0;
	flags_6502 &= ~FC;
	if (data & (1 << 7)) {
		flags_6502 |= FC;
	}
	data = data << 1;
	data += carry;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void ROL_6502x26() {
	//(Z-Page)
	uint8_t address = internal6502_immediate();
	uint8_t data = c64zeropage_6502[address];
	uint8_t carry = (flags_6502 & FC) ? 127 : 0;
	flags_6502 &= ~FC;
	if (data & (1)) {
		flags_6502 |= FC;
	}
	data = data >> 1;
	data += carry;
	c64zeropage_6502[address] = data;
	internal6502_valueflags(data);

}

void ROL_6502x2A() {
	//(Accumulator)
	uint8_t carry = (flags_6502 & FC) ? 127 : 0;
	flags_6502 &= ~FC;
	if (a_6502 & (1)) {
		flags_6502 |= FC;
	}
	a_6502 = a_6502 >> 1;
	a_6502 += carry;
	internal6502_acuflags();

}

void ROL_6502x36() {
	//(Z-Page,X)
	uint8_t address = internal6502_immediate();
	address += x_6502;
	uint8_t data = c64zeropage_6502[address];
	uint8_t carry = (flags_6502 & FC) ? 127 : 0;
	flags_6502 &= ~FC;
	if (data & (1)) {
		flags_6502 |= FC;
	}
	data = data >> 1;
	data += carry;
	c64zeropage_6502[address] = data;
	internal6502_valueflags(data);

}

void ROL_6502x2E() {
	//(Absolute)
	uint16_t address = internal6502_absolute_address();
	uint8_t data = internal6502_readbyte(address);
	uint8_t carry = (flags_6502 & FC) ? 127 : 0;
	flags_6502 &= ~FC;
	if (data & (1)) {
		flags_6502 |= FC;
	}
	data = data >> 1;
	data += carry;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void ROL_6502x3E() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t data = internal6502_readbyte(address);
	uint8_t carry = (flags_6502 & FC) ? 1 : 0;
	flags_6502 &= ~FC;
	if (data & (1)) {
		flags_6502 |= FC;
	}
	data = data >> 1;
	data += carry;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void JAM_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 2;
}

void RLA_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void RRA_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SLO_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SAX_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void ARR_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SHA_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SHS_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SHY_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void LAX_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void LXA_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void LAE_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SHX_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void DCP_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void ISB_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void ANE_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void ANC_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SRE_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void ASR_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SBC_6502xEB() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void SBX_6502() {
	//todo: implement this illegal opcode
	broken_6502 = 1;
}

void NOP_6502_o0() {

}

void NOP_6502_o1() {
	pc_6502++;

}

void NOP_6502_o2() {
	pc_6502++;
	pc_6502++;

}

void ASL_6502x06() {
	pc_6502++;
	flags_6502 &= ~FC;
	//unset carry;
	uint8_t zaddress = internal6502_readbyte(pc_6502);
	pc_6502++;
	uint8_t zvalue = c64zeropage_6502[zaddress];
	if (zvalue & (1 << 7)) {
		flags_6502 |= FC;
	}
	c64zeropage_6502[zaddress] = zvalue << 1;

}

void ASL_6502x0A() {
	pc_6502++;
	flags_6502 &= ~FC;
	if (a_6502 & (1 << 7)) {
		flags_6502 |= FC;
	}
	a_6502 = a_6502 << 1;

}

void ASL_6502x0E() {
	pc_6502++;
	flags_6502 &= ~FC;
	uint16_t maddress = internal6502_absolute_address();
	uint8_t value = internal6502_readbyte(maddress);
	if (value & (1 << 7)) {
		flags_6502 |= FC;
	}
	internal6502_writebyte(maddress, value << 1);

}

void ASL_6502x16() {
	pc_6502++;
	flags_6502 &= ~FC;
	uint8_t data = internal6502_immediate();
	data += x_6502;
	uint8_t value = c64zeropage_6502[data];
	if (value & (1 << 7)) {
		flags_6502 |= FC;
	}
	c64zeropage_6502[data] = (value << 1);

}

void ASL_6502x1E() {
	pc_6502++;
	flags_6502 &= ~FC;
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t value = internal6502_readbyte(address);
	if (value & (1 << 7)) {
		flags_6502 |= FC;
	}
	internal6502_writebyte(address, value << 1);

}

void PHP_6502_ok() {
	internal6502_pushbyte(flags_6502);

}

void PLP_6502_ok() {
	flags_6502 = internal6502_popbyte();

}

void PHA_6502_ok() {
	internal6502_pushbyte(a_6502);

}

void PLA_6502_ok() {
	a_6502 = internal6502_popbyte();

}

void TXS_6502_ok() {
	sp_6502 = x_6502;

}

void TSX_6502_ok() {
	x_6502 = sp_6502;

}

void BPL_6502_ok() {
	if ((flags_6502 & FN) == 0)
		internal6502_branch();

}

void BMI_6502_ok() {
	if ((flags_6502 & FN))
		internal6502_branch();

}

void BVC_6502_ok() {
	if ((flags_6502 & FV) == 0)
		internal6502_branch();

}

void BVS_6502_ok() {
	if ((flags_6502 & FV))
		internal6502_branch();

}

void BCC_6502_ok() {
	if ((flags_6502 & FC) == 0)
		internal6502_branch();

}

void BCS_6502_ok() {
	if ((flags_6502 & FC))
		internal6502_branch();

}

void BNE_6502_ok() {
	if ((flags_6502 & FZ) == 0)
		internal6502_branch();

}

void BEQ_6502_ok() {
	if ((flags_6502 & FZ))
		internal6502_branch();

}

void CLC_6502_ok() {
	flags_6502 &= ~FC;

}

void SEC_6502_ok() {
	flags_6502 |= FC;

}

void CLI_6502_ok() {
	flags_6502 &= ~FI;

}

void SEI_6502_ok() {
	flags_6502 |= FI;

}

void CLV_6502_ok() {
	flags_6502 &= ~FV;

}

void CLD_6502_ok() {
	flags_6502 &= ~FD;

}

void SED_6502_ok() {
	flags_6502 |= FD;

}

void TAX_6502_ok() {
	x_6502 = a_6502;
	internal6502_acuflags();

}

void TXA_6502_ok() {
	a_6502 = x_6502;
	internal6502_acuflags();

}

void TAY_6502_ok() {
	y_6502 = a_6502;
	internal6502_acuflags();

}

void TYA_6502_ok() {
	a_6502 = y_6502;
	internal6502_acuflags();

}

void INX_6502_ok() {
	x_6502++;
	internal6502_valueflags(x_6502);

}

void DEX_6502_ok() {
	x_6502--;
	internal6502_valueflags(x_6502);

}

void INY_6502_ok() {
	y_6502++;
	internal6502_valueflags(y_6502);

}

void DEY_6502_ok() {
	y_6502--;
	internal6502_valueflags(y_6502);

}

void INC_6502xE6() {
	//(Z-Page)
	uint8_t data2 = internal6502_immediate();
	uint8_t data = c64zeropage_6502[data2];
	data++;
	c64zeropage_6502[data2] = data;
	internal6502_valueflags(data);

}

void INC_6502xF6() {
	//(Z-Page,X)
	uint8_t data2 = internal6502_immediate();
	data2 += x_6502;
	uint8_t data = c64zeropage_6502[data2];
	data++;
	c64zeropage_6502[data2] = data;
	internal6502_valueflags(data);

}

void INC_6502xEE() {
	//(Absolute)
	uint16_t address = internal6502_absolute_address();
	uint8_t data = internal6502_readbyte(address);
	data++;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void INC_6502xFE() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t data = internal6502_readbyte(address);
	data++;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void DEC_6502xC6() {
	//(Z-Page)
	uint8_t data2 = internal6502_immediate();
	uint8_t data = c64zeropage_6502[data2];
	data--;
	c64zeropage_6502[data2] = data;
	internal6502_valueflags(data);

}

void DEC_6502xD6() {
	//(Z-Page,X)
	uint8_t data2 = internal6502_immediate();
	data2 += x_6502;
	uint8_t data = c64zeropage_6502[data2];
	data--;
	c64zeropage_6502[data2] = data;
	internal6502_valueflags(data);

}

void DEC_6502xCE() {
	//(Absolute)
	uint16_t address = internal6502_absolute_address();
	uint8_t data = internal6502_readbyte(address);
	data--;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void DEC_6502xDE() {
	//(Absolute,X)
	uint16_t address = internal6502_absolute_address() + x_6502;
	uint8_t data = internal6502_readbyte(address);
	data--;
	internal6502_writebyte(address, data);
	internal6502_valueflags(data);

}

void JSR_6502_ok() {
	uint16_t address = internal6502_absolute_address();
	uint16_t back = internal6502_absolute_address();
	//todo: endianess?
	internal6502_pushword(back - 1);
	pc_6502 = address;

}

void RTS_6502_ok() {
	uint16_t address = internal6502_popword();
	pc_6502 = address - 1;

}

void RTI_6502_ok() {
	flags_6502 = internal6502_popbyte();
	uint16_t address = internal6502_popword();
	pc_6502 = address;

}

void JMP_6502x4C() {
	uint16_t address = internal6502_absolute_address();
	pc_6502 = address;

}

void JMP_6502x6C() {
	uint16_t address = internal6502_absolute_address();
	pc_6502 = internal6502_readword(address);

}

/*
 * the function naming:
 * MNM_6502	-	an illegal opcode, not implemented. will likely break any program
 * MNM_6502xNN - an implemented opcode with mnemonic MNM and hex value NN
 * MNM_6502_ok - an implemented opcode with no variant in addressing
 * NOP_6502_oN - NOP opcode, with N parameters for unused addressing
 */

const fun6502_t opcode6502[] = { // 6502 opcodes following..
		BRK_6502x00, // 0x00       Stack <- PC, PC <- ($fffe) (Immediate)      1/7
				ORA_6502x01, // 0x01       A <- (A) V M               (Ind,X)          6/2
				JAM_6502, // 0x02       [locks up machine]         (Implied)        1/-
				SLO_6502, // 0x03       M <- (M >> 1) + A + C      (Ind,X)          2/8
				NOP_6502_o1, // 0x04       [no operation]             (Z-Page)         2/3
				ORA_6502x05, // 0x05     A <- (A) V M               (Z-Page)         2/3
				ASL_6502x06, // 0x06       C <- A7, A <- (A) << 1     (Z-Page)         2/5
				SLO_6502, // 0x07       M <- (M >> 1) + A + C      (Z-Page)         2/5
				PHP_6502_ok, // 0x08       Stack <- (P)               (Implied)        1/3
				ORA_6502x09, // 0x09       A <- (A) V M               (Immediate)      2/2
				ASL_6502x0A, // 0x0A       C <- A7, A <- (A) << 1     (Accumulator)    1/2
				ANC_6502, // 0x0B       A <- A /\ M, C=~A7         (Immediate)      1/2
				NOP_6502_o2, // 0x0C       [no operation]             (Absolute)       3/4
				ORA_6502x0D, // 0x0D       A <- (A) V M               (Absolute)       3/4
				ASL_6502x0E, // 0x0E       C <- A7, A <- (A) << 1     (Absolute)       3/6
				SLO_6502, // 0x0F       M <- (M >> 1) + A + C      (Absolute)       3/6
				BPL_6502_ok, // 0x10       if N=0, PC = PC + offset   (Relative)       2/2'2
				ORA_6502x11, // 0x11       A <- (A) V M               ((Ind),Y)        2/5'1
				JAM_6502, // 0x12       [locks up machine]         (Implied)        1/-
				SLO_6502, // 0x13       M <- (M >. 1) + A + C      ((Ind),Y)        2/8'5
				NOP_6502_o1, // 0x14       [no operation]             (Z-Page,X)       2/4
				ORA_6502x15, // 0x15       A <- (A) V M               (Z-Page,X)       2/4
				ASL_6502x16, // 0x16       C <- A7, A <- (A) << 1     (Z-Page,X)       2/6
				SLO_6502, // 0x17       M <- (M >> 1) + A + C      (Z-Page,X)       2/6
				CLC_6502_ok, // 0x18       C <- 0                     (Implied)        1/2
				ORA_6502x19, // 0x19       A <- (A) V M               (Absolute,Y)     3/4'1
				NOP_6502_o0, // 0x1A       [no operation]             (Implied)        1/2
				SLO_6502, // 0x1B       M <- (M >> 1) + A + C      (Absolute,Y)     3/7
				NOP_6502_o2, // 0x1C       [no operation]             (Absolute,X)     2/4'1
				ORA_6502x1D, // 0x1D       A <- (A) V M               (Absolute,X)     3/4'1
				ASL_6502x1E, // 0x1E       C <- A7, A <- (A) << 1     (Absolute,X)     3/7
				SLO_6502, // 0x1F       M <- (M >> 1) + A + C      (Absolute,X)     3/7
				JSR_6502_ok, // 0x20       Stack <- PC, PC <- Address (Absolute)       3/6
				AND_6502x21, // 0x21       A <- (A) /\ M              (Ind,X)          2/6
				JAM_6502, // 0x22       [locks up machine]         (Implied)        1/-
				RLA_6502, // 0x23       M <- (M << 1) /\ (A)       (Ind,X)          2/8
				BIT_6502x24, // 0x24       Z <- ~(A /\ M) N<-M7 V<-M6 (Z-Page)         2/3
				AND_6502x25, // 0x25       A <- (A) /\ M              (Z-Page)         2/3
				ROL_6502x26, // 0x26       C <- A7 & A <- A << 1 + C  (Z-Page)         2/5
				RLA_6502, // 0x27       M <- (M << 1) /\ (A)       (Z-Page)         2/5'5
				PLP_6502_ok, // 0x28       A <- (Stack)               (Implied)        1/4
				AND_6502x29, // 0x29       A <- (A) /\ M              (Immediate)      2/2
				ROL_6502x2A, // 0x2A       C <- A7 & A <- A << 1 + C  (Accumalator)    1/2
				ANC_6502, // 0x2B       A <- A /\ M, C <- ~A7      (Immediate9      1/2
				BIT_6502x2C, // 0x2C       Z <- ~(A /\ M) N<-M7 V<-M6 (Absolute)       3/4
				AND_6502x2D, // 0x2D       A <- (A) /\ M              (Absolute)       3/4
				ROL_6502x2E, // 0x2E       C <- A7 & A <- A << 1 + C  (Absolute)       3/6
				RLA_6502, // 0x2F       M <- (M << 1) /\ (A)       (Absolute)       3/6'5
				BMI_6502_ok, // 0x30       if N=1, PC = PC + offset   (Relative)       2/2'2
				AND_6502x31, // 0x31       A <- (A) /\ M              ((Ind),Y)        2/5'1
				JAM_6502, // 0x32       [locks up machine]         (Implied)        1/-
				RLA_6502, // 0x33       M <- (M << 1) /\ (A)       ((Ind),Y)        2/8'5
				NOP_6502_o1, // 0x34       [no operation]             (Z-Page,X)       2/4
				AND_6502x35, // 0x35       A <- (A) /\ M              (Z-Page,X)       2/4
				ROL_6502x36, // 0x36       C <- A7 & A <- A << 1 + C  (Z-Page,X)       2/6
				RLA_6502, // 0x37       M <- (M << 1) /\ (A)       (Z-Page,X)       2/6'5
				SEC_6502_ok, // 0x38       C <- 1                     (Implied)        1/2
				AND_6502x39, // 0x39       A <- (A) /\ M              (Absolute,Y)     3/4'1
				NOP_6502_o0, // 0x3A       [no operation]             (Implied)        1/2
				RLA_6502, // 0x3B       M <- (M << 1) /\ (A)       (Absolute,Y)     3/7'5
				NOP_6502_o2, // 0x3C       [no operation]             (Absolute,X)     3/4'1
				AND_6502x3D, // 0x3D       A <- (A) /\ M              (Absolute,X)     3/4'1
				ROL_6502x3E, // 0x3E       C <- A7 & A <- A << 1 + C  (Absolute,X)     3/7
				RLA_6502, // 0x3F       M <- (M << 1) /\ (A)       (Absolute,X)     3/7'5
				RTI_6502_ok, // 0x40       P <- (Stack), PC <-(Stack) (Implied)        1/6
				EOR_6502x41, // 0x41       A <- (A) \-/ M             (Ind,X)          2/6
				JAM_6502, // 0x42       [locks up machine]         (Implied)        1/-
				SRE_6502, // 0x43       M <- (M >> 1) \-/ A        (Ind,X)          2/8
				NOP_6502_o1, // 0x44       [no operation]             (Z-Page)         2/3
				EOR_6502x45, // 0x45       A <- (A) \-/ M             (Z-Page)         2/3
				LSR_6502x46, // 0x46       C <- A0, A <- (A) >> 1     (Absolute,X)     3/7
				SRE_6502, // 0x47       M <- (M >> 1) \-/ A        (Z-Page)         2/5
				PHA_6502_ok, // 0x48       Stack <- (A)               (Implied)        1/3
				EOR_6502x49, // 0x49       A <- (A) \-/ M             (Immediate)      2/2
				LSR_6502x4A, // 0x4A       C <- A0, A <- (A) >> 1     (Accumalator)    1/2
				ASR_6502, // 0x4B       A <- [(A /\ M) >> 1]       (Immediate)      1/2
				JMP_6502x4C, // 0x4C       PC <- Address              (Absolute)       3/3
				EOR_6502x4D, // 0x4D       A <- (A) \-/ M             (Absolute)       3/4
				LSR_6502x4E, // 0x4E       C <- A0, A <- (A) >> 1     (Absolute)       3/6
				SRE_6502, // 0x4F       M <- (M >> 1) \-/ A        (Absolute)       3/6
				BVC_6502_ok, // 0x50       if V=0, PC = PC + offset   (Relative)       2/2'2
				EOR_6502x51, // 0x51       A <- (A) \-/ M             ((Ind),Y)        2/5'1
				JAM_6502, // 0x52       [locks up machine]         (Implied)        1/-
				SRE_6502, // 0x53       M <- (M >> 1) \-/ A        ((Ind),Y)        2/8
				NOP_6502_o1, // 0x54       [no operation]             (Z-Page,X)       2/4
				EOR_6502x55, // 0x55       A <- (A) \-/ M             (Z-Page,X)       2/4
				LSR_6502x56, // 0x56       C <- A0, A <- (A) >> 1     (Z-Page,X)       2/6
				SRE_6502, // 0x57       M <- (M >> 1) \-/ A        (Z-Page,X)       2/6
				CLI_6502_ok, // 0x58       I <- 0                     (Implied)        1/2
				EOR_6502x59, // 0x59       A <- (A) \-/ M             (Absolute,Y)     3/4'1
				NOP_6502_o0, // 0x5A       [no operation]             (Implied)        1/2
				SRE_6502, // 0x5B       M <- (M >> 1) \-/ A        (Absolute,Y)     3/7
				NOP_6502_o2, // 0x5C       [no operation]             (Absolute,X)     3/4'1
				EOR_6502x5D, // 0x5D       A <- (A) \-/ M             (Absolute,X)     3/4'1
				SRE_6502, // 0x5F       M <- (M >> 1) \-/ A        (Absolute,X)     3/7
				RTS_6502_ok, // 0x60       PC <- (Stack)              (Implied)        1/6
				ADC_6502x61, // 0x61       A <- (A) + M + C           (Ind,X)          2/6
				JAM_6502, // 0x62       [locks up machine]         (Implied)        1/-
				RRA_6502, // 0x63       M <- (M >> 1) + (A) + C    (Ind,X)          2/8'5
				NOP_6502_o1, // 0x64       [no operation]             (Z-Page)         2/3
				ADC_6502x65, // 0x65       A <- (A) + M + C           (Z-Page)         2/3
				ROR_6502x66, // 0x66       C<-A0 & A<- (A7=C + A>>1)  (Z-Page)         2/5
				RRA_6502, // 0x67       M <- (M >> 1) + (A) + C    (Z-Page)         2/5'5
				PLA_6502_ok, // 0x68       A <- (Stack)               (Implied)        1/4
				ADC_6502x69, // 0x69       A <- (A) + M + C           (Immediate)      2/2
				ROR_6502x6A, // 0x6A       C<-A0 & A<- (A7=C + A>>1)  (Accumalator)    1/2
				ARR_6502, // 0x6B       A <- [(A /\ M) >> 1]       (Immediate)      1/2'5
				JMP_6502x6C, // 0x6C       PC <- Address              (Indirect)       3/5
				ADC_6502x6D, // 0x6D       A <- (A) + M + C           (Absolute)       3/4
				ROR_6502x6E, // 0x6E       C<-A0 & A<- (A7=C + A>>1)  (Absolute)       3/6
				RRA_6502, // 0x6F       M <- (M >> 1) + (A) + C    (Absolute)       3/6'5
				BVS_6502_ok, // 0x70       if V=1, PC = PC + offset   (Relative)       2/2'2
				ADC_6502x71, // 0x71       A <- (A) + M + C           ((Ind),Y)        2/5'1
				JAM_6502, // 0x72       [locks up machine]         (Implied)        1/-
				RRA_6502, // 0x73       M <- (M >> 1) + (A) + C    ((Ind),Y)        2/8'5
				NOP_6502_o1, // 0x74       [no operation]             (Z-Page,X)       2/4
				ADC_6502x75, // 0x75       A <- (A) + M + C           (Z-Page,X)       2/4
				ROR_6502x76, // 0x76       C<-A0 & A<- (A7=C + A>>1)  (Z-Page,X)       2/6
				RRA_6502, // 0x77       M <- (M >> 1) + (A) + C    (Z-Page,X)       2/6'5
				SEI_6502_ok, // 0x78       I <- 1                     (Implied)        1/2
				ADC_6502x79, // 0x79       A <- (A) + M + C           (Absolute,Y)     3/4'1
				NOP_6502_o0, // 0x7A       [no operation]             (Implied)        1/2
				RRA_6502, // 0x7B       M <- (M >> 1) + (A) + C    (Absolute,Y)     3/7'5
				NOP_6502_o2, // 0x7C       [no operation]             (Absolute,X)     3/4'1
				ADC_6502x7D, // 0x7D       A <- (A) + M + C           (Absolute,X)     3/4'1
				ROR_6502x7E, // 0x7E       C<-A0 & A<- (A7=C + A>>1)  (Absolute,X)     3/7
				RRA_6502, // 0x7F       M <- (M >> 1) + (A) + C    (Absolute,X)     3/7'5
				NOP_6502_o1, // 0x80       [no operation]             (Immediate)      2/2
				STA_6502x81, // 0x81       M <- (A)                   (Ind,X)          2/6
				NOP_6502_o1, // 0x82       [no operation]             (Immediate)      2/2
				SAX_6502, // 0x83       M <- (A) /\ (X)            (Ind,X)          2/6
				STY_6502x84, // 0x84       M <- (Y)                   (Z-Page)         2/3
				STA_6502x85, // 0x85       M <- (A)                   (Z-Page)         2/3
				STX_6502x86, // 0x86       M <- (X)                   (Z-Page)         2/3
				SAX_6502, // 0x87       M <- (A) /\ (X)            (Z-Page)         2/3
				DEY_6502_ok, // 0x88       Y <- (Y) - 1               (Implied)        1/2
				NOP_6502_o1, // 0x89       [no operation]             (Immediate)      2/2
				TXA_6502_ok, // 0x8A       A <- (X)                   (Implied)        1/2
				ANE_6502, // 0x8B       M <-[(A)\/$EE] /\ (X)/\(M) (Immediate)      2/2^4
				STY_6502x8C, // 0x8C       M <- (Y)                   (Absolute)       3/4
				STA_6502x8D, // 0x8D       M <- (A)                   (Absolute)       3/4
				STX_6502x8E, // 0x8E       M <- (X)                   (Absolute)       3/4
				SAX_6502, // 0x8F       M <- (A) /\ (X)            (Absolute)       3/4
				BCC_6502_ok, // 0x90       if C=0, PC = PC + offset   (Relative)       2/2'2
				STA_6502x91, // 0x91       M <- (A)                   ((Ind),Y)        2/6
				JAM_6502, // 0x92       [locks up machine]         (Implied)        1/-
				SHA_6502, // 0x93       M <- (A) /\ (X) /\ (PCH+1) (Absolute,X)     3/6'3
				STY_6502x94, // 0x94       M <- (Y)                   (Z-Page,X)       2/4
				STA_6502x95, // 0x95       M <- (A)                   (Z-Page,X)       2/4
				STX_6502x96, // 0x96       M <- (X)                   (Z-Page,Y)       2/4
				SAX_6502, // 0x97       M <- (A) /\ (X)            (Z-Page,Y)       2/4
				TYA_6502_ok, // 0x98       A <- (Y)                   (Implied)        1/2
				STA_6502x99, // 0x99       M <- (A)                   (Absolute,Y)     3/5
				TXS_6502_ok, // 0x9A       S <- (X)                   (Implied)        1/2
				SHS_6502, // 0x9B       X <- (A) /\ (X), S <- (X),  M <- (X) /\ (PCH+1)       (Absolute,Y)     3/5
				SHY_6502, // 0x9C       M <- (Y) /\ (PCH+1)        (Absolute,Y)     3/5'3
				STA_6502x9D, // 0x9D       M <- (A)                   (Absolute,X)     3/5
				SHX_6502, // 0x9E       M <- (X) /\ (PCH+1)        (Absolute,X)     3/5'3
				SHA_6502, // 0x9F       M <- (A) /\ (X) /\ (PCH+1) (Absolute,Y)     3/5'3
				LDY_6502xA0, // 0xA0       Y <- M                     (Immediate)      2/2
				LDA_6502xA1, // 0xA1       A <- M                     (Ind,X)          2/6
				LDX_6502xA2, // 0xA2       X <- M                     (Immediate)      2/2
				LAX_6502, // 0xA3       A <- M, X <- M             (Ind,X)          2/6
				LDY_6502xA4, // 0xA4       Y <- M                     (Z-Page)         2/3
				LDA_6502xA5, // 0xA5       A <- M                     (Z-Page)         2/3
				LDX_6502xA6, // 0xA6       X <- M                     (Z-Page)         2/3
				LAX_6502, // 0xA7       A <- M, X <- M             (Z-Page)         2/3
				TAY_6502_ok, // 0xA8       Y <- (A)                   (Implied)        1/2
				LDA_6502xA9, // 0xA9       A <- M                     (Immediate)      2/2
				TAX_6502_ok, // 0xAA       X <- (A)                   (Implied)        1/2
				LXA_6502, // 0xAB       X04 <- (X04) /\ M04, A04 <- (A04) /\ M04        (Immediate)      1/2
				LDY_6502xAC, // 0xAC       Y <- M                     (Absolute)       3/4
				LDA_6502xAD, // 0xAD       A <- M                     (Absolute)       3/4
				LDX_6502xAE, // 0xAE       X <- M                     (Absolute)       3/4
				LAX_6502, // 0xAF       A <- M, X <- M             (Absolute)       3/4
				BCS_6502_ok, // 0xB0       if C=1, PC = PC + offset   (Relative)       2/2'2
				LDA_6502xB1, // 0xB1       A <- M                     ((Ind),Y)        2/5'1
				JAM_6502, // 0xB2       [locks up machine]         (Implied)        1/-
				LAX_6502, // 0xB3       A <- M, X <- M             ((Ind),Y)        2/5'1
				LDY_6502xB4, // 0xB4       Y <- M                     (Z-Page,X)       2/4
				LDA_6502xB5, // 0xB5       A <- M                     (Z-Page,X)       2/4
				LDX_6502xB6, // 0xB6       X <- M                     (Z-Page,Y)       2/4
				LAX_6502, // 0xB7       A <- M, X <- M             (Z-Page,Y)       2/4
				CLV_6502_ok, // 0xB8       V <- 0                     (Implied)        1/2
				LDA_6502xB9, // 0xB9       A <- M                     (Absolute,Y)     3/4'1
				TSX_6502_ok, // 0xBA       X <- (S)                   (Implied)        1/2
				LAE_6502, // 0xBB       X,S,A <- (S /\ M)          (Absolute,Y)     3/4'1
				LDY_6502xBC, // 0xBC       Y <- M                     (Absolute,X)     3/4'1
				LDA_6502xBD, // 0xBD       A <- M                     (Absolute,X)     3/4'1
				LDX_6502xBE, // 0xBE       X <- M                     (Absolute,Y)     3/4'1
				LAX_6502, // 0xBF       A <- M, X <- M             (Absolute,Y)     3/4'1
				CPY_6502xC0, // 0xC0       (Y - M) -> NZC             (Immediate)      2/2
				CMP_6502xC1, // 0xC1       (A - M) -> NZC             (Ind,X)          2/6
				NOP_6502_o1, // 0xC2       [no operation]             (Immediate)      2/2
				DCP_6502, // 0xC3       M <- (M)-1, (A-M) -> NZC   (Ind,X)          2/8
				CPY_6502xC4, // 0xC4       (Y - M) -> NZC             (Z-Page)         2/3
				CMP_6502xC5, // 0xC5       (A - M) -> NZC             (Z-Page)         2/3
				DEC_6502xC6, // 0xC6       M <- (M) - 1               (Z-Page)         2/5
				DCP_6502, // 0xC7       M <- (M)-1, (A-M) -> NZC   (Z-Page)         2/5
				INY_6502_ok, // 0xC8       Y <- (Y) + 1               (Implied)        1/2
				CMP_6502xC9, // 0xC9       (A - M) -> NZC             (Immediate)      2/2
				DEX_6502_ok, // 0xCA       X <- (X) - 1               (Implied)        1/2
				SBX_6502, // 0xCB       X <- (X)/\(A) - M          (Immediate)      2/2
				CPY_6502xCC, // 0xCC       (Y - M) -> NZC             (Absolute)       3/4
				CMP_6502xCD, // 0xCD       (A - M) -> NZC             (Absolute)       3/4
				DEC_6502xCE, // 0xCE       M <- (M) - 1               (Absolute)       3/6
				DCP_6502, // 0xCF       M <- (M)-1, (A-M) -> NZC   (Absolute)       3/6
				BNE_6502_ok, // 0xD0       if Z=0, PC = PC + offset   (Relative)       2/2'2
				CMP_6502xD1, // 0xD1       (A - M) -> NZC             ((Ind),Y)        2/5'1
				JAM_6502, // 0xD2       [locks up machine]         (Implied)        1/-
				DCP_6502, // 0xD3       M <- (M)-1, (A-M) -> NZC   ((Ind),Y)        2/8
				NOP_6502_o1, // 0xD4       [no operation]             (Z-Page,X)       2/4
				CMP_6502xD5, // 0xD5       (A - M) -> NZC             (Z-Page,X)       2/4
				DEC_6502xD6, // 0xD6       M <- (M) - 1               (Z-Page,X)       2/6
				DCP_6502, // 0xD7       M <- (M)-1, (A-M) -> NZC   (Z-Page,X)       2/6
				CLD_6502_ok, // 0xD8       D <- 0                     (Implied)        1/2
				CMP_6502xD9, // 0xD9       (A - M) -> NZC             (Absolute,Y)     3/4'1
				NOP_6502_o0, // 0xDA       [no operation]             (Implied)        1/2
				DCP_6502, // 0xDB       M <- (M)-1, (A-M) -> NZC   (Absolute,Y)     3/7
				NOP_6502_o2, // 0xDC       [no operation]             (Absolute,X)     3/4'1
				CMP_6502xDD, // 0xDD       (A - M) -> NZC             (Absolute,X)     3/4'1
				DEC_6502xDE, // 0xDE       M <- (M) - 1               (Absolute,X)     3/7
				DCP_6502, // 0xDF       M <- (M)-1, (A-M) -> NZC   (Absolute,X)     3/7
				CPX_6502xE0, // 0xE0       (X - M) -> NZC             (Immediate)      2/2
				SBC_6502xE1, // 0xE1       A <- (A) - M - ~C          (Ind,X)          2/6
				NOP_6502_o1, // 0xE2       [no operation]             (Immediate)      2/2
				ISB_6502, // 0xE3       M <- (M) - 1,A <- (A)-M-~C (Ind,X)          3/8'1
				CPX_6502xE4, // 0xE4       (X - M) -> NZC             (Z-Page)         2/3
				SBC_6502xE5, // 0xE5       A <- (A) - M - ~C          (Z-Page)         2/3
				INC_6502xE6, // 0xE6       M <- (M) + 1               (Z-Page)         2/5
				ISB_6502, // 0xE7       M <- (M) - 1,A <- (A)-M-~C (Z-Page)         2/5
				INX_6502_ok, // 0xE8       X <- (X) +1                (Implied)        1/2
				SBC_6502xE9, // 0xE9       A <- (A) - M - ~C          (Immediate)      2/2
				NOP_6502_o0, // 0xEA       [no operation]             (Implied)        1/2
				SBC_6502xEB, // 0xEB       A <- (A) - M - ~C          (Immediate)      1/2
				CPX_6502xEC, // 0xEC       (X - M) -> NZC             (Absolute)       3/4
				SBC_6502xED, // 0xED       A <- (A) - M - ~C          (Absolute)       3/4
				INC_6502xEE, // 0xEE       M <- (M) + 1               (Absolute)       3/6
				ISB_6502, // 0xEF       M <- (M) - 1,A <- (A)-M-~C (Absolute)       3/6
				BEQ_6502_ok, // 0xF0       if Z=1, PC = PC + offset   (Relative)       2/2'2
				SBC_6502xF1, // 0xF1       A <- (A) - M - ~C          ((Ind),Y)        2/5'1
				JAM_6502, // 0xF2       [locks up machine]         (Implied)        1/-
				ISB_6502, // 0xF3       M <- (M) - 1,A <- (A)-M-~C ((Ind),Y)        2/8
				NOP_6502_o1, // 0xF4       [no operation]             (Z-Page,X)       2/4
				SBC_6502xF5, // 0xF5       A <- (A) - M - ~C          (Z-Page,X)       2/4
				INC_6502xF6, // 0xF6       M <- (M) + 1               (Z-Page,X)       2/6
				ISB_6502, // 0xF7       M <- (M) - 1,A <- (A)-M-~C (Z-Page,X)       2/6
				SED_6502_ok, // 0xF8       D <- 1                     (Implied)        1/2
				SBC_6502xF9, // 0xF9       A <- (A) - M - ~C          (Absolute,Y)     3/4'1
				NOP_6502_o0, // 0xFA       [no operation]             (Implied)        1/2
				ISB_6502, // 0xFB       M <- (M) - 1,A <- (A)-M-~C (Absolute,Y)     3/7
				NOP_6502_o2, // 0xFC       [no operation]             (Absolute,X)     3/4'1
				SBC_6502xFD, // 0xFD       A <- (A) - M - ~C          (Absolute,X)     3/4'1
				INC_6502xFE, // 0xFE       M <- (M) + 1               (Absolute,X)     3/7
				ISB_6502 // 0xFF       M <- (M) - 1,A <- (A)-M-~C (Absolute,X)     3/7
		};

void init6502(uint16_t newpc, uint8_t newa, uint8_t newx, uint8_t newy) {
	sp_6502 = 255;
	pc_6502 = newpc;
	a_6502 = newa;
	x_6502 = newx;
	y_6502 = newy;
	broken_6502 = 0;
}
uint8_t run6502() {
	uint8_t command = internal6502_readbyte(pc_6502);
	pc_6502++;
	opcode6502[command]();
	return broken_6502;
}
