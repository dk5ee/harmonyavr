/*
 * main.c
 *
 */

#include <avr/io.h>
#include <util/delay.h>
#include "defines.h"
#include "midilib.h"
#include "sidfunctions.h"
#include "DAC8bit.h"
#include "effects.h"
#include <avr/pgmspace.h>

const char text0[] PROGMEM = "https://gitlab.com/dk5ee/harmony 2020";

void testloop(uint8_t i) {
	switch (i) {
	case 0:
		LED1on
		sidsetnote(0,60);
		sidsetgate(0,1);
		break;
	case 1:
		LED2on
		sidsetnote(1,64);
		sidsetgate(1,1);
		break;
	case 2:
		LED1off
		sidsetnote(2,67);
		sidsetgate(2,1);
		break;
	default:
		sidsetmode(15); //volume to max
		sidsetgate(0,0);
		sidsetgate(1,0);
		sidsetgate(2,0);
		LED2off
		break;
	}
}

void loopie() {
	for (uint8_t i = 0; i < 50; i++) {
		midiinparse();
		_delay_ms(10);
	}
}

int main(void) {
	midiinit();
	sidinit();

	LEDinit
	while (1) {
		for (int i = 0; i < 4; i++) {
			testloop(i);
			loopie();
		}

	}
	return 0;
}
