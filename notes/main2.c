#include <avr/io.h>
#include <util/delay.h>
/* pin description
 * 			0	1	2	3	4	5	6	7
 * portb: 	_w	d0	d1	ack dat clk -   -
 * portc:	d2	d3	d4	d5	d6	d7  -   -
 * portd:	rx	tx	a0	a1	a2	a3	a4	_cs
 */
/*
const uint16_t NOTE_TABLE[128] = { 41, // #d -0
		43, // e -0
		46, // #e -0
		48, // f 0
		51, // g 0
		54, // #g 0
		58, // a 0
		61, // #a 0
		65, // h 0
		69, // c 1
		73, // #c 1
		77, // d 1
		82, // #d 1
		86, // e 1
		92, // #e 1
		97, // f 1
		103, // g 1
		109, // #g 1
		115, // a 1
		122, // #a 1
		129, // h 1
		137, // c 2
		145, // #c 2
		154, // d 2
		163, // #d 2
		173, // e 2
		183, // #e 2
		194, // f 2
		206, // g 2
		218, // #g 2
		231, // a 2
		244, // #a 2
		259, // h 2
		274, // c 3
		291, // #c 3
		308, // d 3
		326, // #d 3
		346, // e 3
		366, // #e 3
		388, // f 3
		411, // g 3
		435, // #g 3
		461, // a 3
		489, // #a 3
		518, // h 3
		549, // c 4
		581, // #c 4
		616, // d 4
		652, // #d 4
		691, // e 4
		732, // #e 4
		776, // f 4
		822, // g 4
		871, // #g 4
		923, // a 4
		978, // #a 4
		1036, // h 4
		1097, // c 5
		1163, // #c 5
		1232, // d 5
		1305, // #d 5
		1383, // e 5
		1465, // #e 5
		1552, // f 5
		1644, // g 5
		1742, // #g 5
		1845, // a 5
		1955, // #a 5
		2071, // h 5
		2195, // c 6
		2325, // #c 6
		2463, // d 6
		2610, // #d 6
		2765, // e 6
		2930, // #e 6
		3104, // f 6
		3288, // g 6
		3484, // #g 6
		3691, // a 6
		3910, // #a 6
		4143, // h 6
		4389, // c 7
		4650, // #c 7
		4927, // d 7
		5220, // #d 7
		5530, // e 7
		5859, // #e 7
		6207, // f 7
		6577, // g 7
		6968, // #g 7
		7382, // a 7
		7821, // #a 7
		8286, // h 7
		8779, // c 8
		9301, // #c 8
		9854, // d 8
		10440, // #d 8
		11060, // e 8
		11718, // #e 8
		12415, // f 8
		13153, // g 8
		13935, // #g 8
		14764, // a 8
		15642, // #a 8
		16572, // h 8
		17557, // c 9
		18601, // #c 9
		19708, // d 9
		20879, // #d 9
		22121, // e 9
		23436, // #e 9
		24830, // f 9
		26306, // g 9
		27871, // #g 9
		29528, // a 9
		31284, // #a 9
		33144, // h 9
		35115, // c 10
		37203, // #c 10
		39415, // d 10
		41759, // #d 10
		44242, // e 10
		46873, // #e 10
		49660, // f 10
		52613, // g 10
		55741, // #g 10
		59056, // a 10
		62567 // #a 10
		};
*/

volatile uint8_t mastertaktstate = 0;
volatile uint8_t slaveackstate = 0;
volatile uint16_t mydata = 0;

const uint8_t decode[] = { 0, 1, 2, 3, 4, 5, 6, 7, 255, 8, 9, 10, 11, 12, 13,
		14, 255, 255, 15, 16, 17, 18, 19, 20, 255, 21, 22, 23, 24, 25, 26, 27,
		255, 255, 255, 255, 28, 29, 30, 31, 255, 32, 33, 34, 35, 36, 37, 38,
		255, 255, 39, 40, 41, 42, 43, 44, 255, 45, 46, 47, 48, 49, 50, 51, 255,
		255, 255, 255, 255, 255, 255, 255, 255, 52, 53, 54, 55, 56, 57, 58,
		255, 255, 59, 60, 61, 62, 63, 64, 255, 65, 66, 67, 68, 69, 70, 71, 255,
		255, 255, 255, 72, 73, 74, 75, 255, 76, 77, 78, 79, 80, 81, 82, 255,
		255, 83, 84, 85, 86, 87, 88, 255, 89, 90, 91, 92, 93, 94, 95, 255, 255,
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 96, 97, 98, 99, 100, 101, 255, 102, 103, 104, 105, 106, 107,
		108, 255, 255, 255, 255, 109, 110, 111, 112, 255, 113, 114, 115, 116,
		117, 118, 119, 255, 255, 120, 121, 122, 123, 124, 125, 255, 126, 127,
		128, 129, 130, 131, 132, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		133, 134, 135, 136, 137, 138, 139, 255, 255, 140, 141, 142, 143, 144,
		145, 255, 146, 147, 148, 149, 150, 151, 152, 255, 255, 255, 255, 153,
		154, 155, 156, 255, 157, 158, 159, 160, 161, 162, 163, 255, 255, 164,
		165, 166, 167, 168, 169, 255, 170, 171, 172, 173, 174, 175, 176 };
uint8_t regs[25] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0 };

void getbit() {
	//schaun ob taktbit sich änderte..

	if (mastertaktstate != (PINB & (1 << 5))) {
		mastertaktstate = PINB & (1 << 5);
		//daten holen..
		mydata <<= 1;
		if (PINB & (1 << 4))
			mydata++;

		//ack ändern
		if (slaveackstate) {
			PORTB &= ~(1 << 3);
			slaveackstate = 0;
		} else {
			PORTB |= (1 << 3);
			slaveackstate = 1;
		}
	}
}
uint8_t dataready() {
	return ((mydata & 15) == 8);
}

uint8_t getdata() {
	int data = mydata;

	mydata = 0;
	return decode[data >> 4];
}

//output one sid register
void SIDout(uint8_t addi, uint8_t data) {
	uint8_t b = (data & 3) << 1; //unterste zwei bit von data, -> b1 und b2
	uint8_t c = (data >> 2) & 63; //obere 6 bit von data ->c0..c5
	uint8_t d = (addi & 31) << 2; //5 bit addi -> d2..d6
	PORTD = d + 128; //cs setzen
	PORTB = (PORTB & 0b11111000) | b;
	PORTC = c;
	PORTD = d; //cs off -> daten bitte annehmen

	//nun: 16 takte warten = 1 takt @ 1mhz
	asm volatile (
			"nop" "\n\t" //1
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t" //16
	);
	PORTD = d + 128; //cs wieder setzen
	PORTB |= (1 << 0); //auf read setzen
	asm volatile (
			"nop" "\n\t" //1
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			/*
			 "nop" "\n\t"
			 "nop" "\n\t"
			 "nop" "\n\t"
			 "nop" "\n\t"
			 "nop" "\n\t"
			 "nop" "\n\t"
			 "nop" "\n\t"
			 "nop" "\n\t"
			 */
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t" //16
	);

	//    _delay_ms(1);
}
void initsid() {
	DDRB = 0xff;
	DDRB = ~((1 << 4) | (1 << 5));// pb4 and pb5 as input
	mastertaktstate = PINB & (1 << 5);
	PORTB |= (1 << 0); //auf read setzen
	DDRD = 0xff;
	PORTD = 128;
	DDRC = 0xff;
	PORTC = 0;
	//_delay_ms(500); //dem sid eine halbe sekunde zeit geben
}

/*
 void setnote(uint8_t bank, uint8_t note) {
 if (bank < 3) {
 uint16_t n = NOTE_TABLE[note + 36]; //frequenz aus tabelle holen
 SIDout(bank * 7 + 0, n & 255); //note low
 SIDout(bank * 7 + 1, n >> 8); //note high
 }
 }
 void bankon(uint8_t bank) {
 if (bank < 3) {
 SIDout(bank * 7 + 4,  1);
 }
 }
 void bankoff(uint8_t bank) {
 if (bank < 3) {
 SIDout(bank * 7 + 4, 0);
 }
 }
 */

void clearsid() {
	uint8_t i = 0;
	for (i = 0; i < 25; i++) {
		SIDout(i, 0);
		regs[i] = 0;
	}
	/*initbank(0);
	 initbank(1);
	 initbank(2);*/
}
/*
 void waitnops() {
 asm volatile (
 "nop" "\n\t" //1
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t" //16
 "nop" "\n\t" //1
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t" //16
 "nop" "\n\t" //1
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t"
 "nop" "\n\t" //16
 );
 }
 */
uint8_t getbyte() {
	do {
		getbit();
		while (((mydata & 15) != 8) && (mydata > 0))
			getbit();
	} while ((mydata & 15) != 8);
	return getdata();
}

int main(void) {
	_delay_ms(500);
	initsid();
	clearsid();
	_delay_ms(500);
	do {
		uint8_t command = getbyte();
		uint8_t targetbit = command >> 2;
		if ((command & 0b00000010) == 0) {
			//single command
			/*
			uint8_t cbit = (command & 0b00000001);
			uint8_t shift = (targetbit & 0b00000111);
			uint8_t dest = targetbit >> 2;
			switch (dest) {
			case 0:
			case 1:
				regs[4] = (regs[4] & ~(1 << shift)) | (cbit << 0);
				SIDout(4, regs[4]);
				break;
			case 2:
			case 3:
				regs[11] = (regs[11] & ~(1 << shift)) | (cbit << 0);
				SIDout(11, regs[11]);
				break;
			case 4:
			case 5:
				regs[18] = (regs[18] & ~(1 << shift)) | (cbit << 0);
				SIDout(18, regs[18]);
				break;
			case 6:
				regs[23] = (regs[23] & ~(1 << shift)) | (cbit << 0);
				SIDout(23, regs[23]);
				break;
			case 7:
				regs[24] = (regs[24] & ~(1 << shift)) | (cbit << 0);
				SIDout(24, regs[24]);
				break;
			default:
				break;
			}
/  same, but longer:*/
			switch (targetbit) {
			case 0://
				regs[4] = (regs[4] & ~(1 << 0)) | ((command & 0b00000001) << 0);
				SIDout(4, regs[4]);
				break;
			case 1://
				regs[4] = (regs[4] & ~(1 << 1)) | ((command & 0b00000001) << 1);
				SIDout(4, regs[4]);
				break;
			case 2://
				regs[4] = (regs[4] & ~(1 << 2)) | ((command & 0b00000001) << 2);
				SIDout(4, regs[4]);
				break;
			case 3://
				regs[4] = (regs[4] & ~(1 << 3)) | ((command & 0b00000001) << 3);
				SIDout(4, regs[4]);
				break;
			case 4://
				regs[4] = (regs[4] & ~(1 << 4)) | ((command & 0b00000001) << 4);
				SIDout(4, regs[4]);
				break;
			case 5://
				regs[4] = (regs[4] & ~(1 << 5)) | ((command & 0b00000001) << 5);
				SIDout(4, regs[4]);
				break;
			case 6://
				regs[4] = (regs[4] & ~(1 << 6)) | ((command & 0b00000001) << 6);
				SIDout(4, regs[4]);
				break;
			case 7://
				regs[4] = (regs[4] & ~(1 << 7)) | ((command & 0b00000001) << 7);
				SIDout(4, regs[4]);
				break;
			case 8://
				regs[11] = (regs[11] & ~(1 << 0)) | ((command & 0b00000001)
						<< 0);
				SIDout(11, regs[11]);
				break;
			case 9://
				regs[11] = (regs[11] & ~(1 << 1)) | ((command & 0b00000001)
						<< 1);
				SIDout(11, regs[11]);
				break;
			case 10://
				regs[11] = (regs[11] & ~(1 << 2)) | ((command & 0b00000001)
						<< 2);
				SIDout(11, regs[11]);
				break;
			case 11://
				regs[11] = (regs[11] & ~(1 << 3)) | ((command & 0b00000001)
						<< 3);
				SIDout(11, regs[11]);
				break;
			case 12://
				regs[11] = (regs[11] & ~(1 << 4)) | ((command & 0b00000001)
						<< 4);
				SIDout(11, regs[11]);
				break;
			case 13://
				regs[11] = (regs[11] & ~(1 << 5)) | ((command & 0b00000001)
						<< 5);
				SIDout(11, regs[11]);
				break;
			case 14://
				regs[11] = (regs[11] & ~(1 << 6)) | ((command & 0b00000001)
						<< 6);
				SIDout(11, regs[11]);
				break;
			case 15://
				regs[11] = (regs[11] & ~(1 << 7)) | ((command & 0b00000001)
						<< 7);
				SIDout(11, regs[11]);
				break;
			case 16://
				regs[18] = (regs[18] & ~(1 << 0)) | ((command & 0b00000001)
						<< 0);
				SIDout(18, regs[18]);
				break;
			case 17://
				regs[18] = (regs[18] & ~(1 << 1)) | ((command & 0b00000001)
						<< 1);
				SIDout(18, regs[18]);
				break;
			case 18://
				regs[18] = (regs[18] & ~(1 << 2)) | ((command & 0b00000001)
						<< 2);
				SIDout(18, regs[18]);
				break;
			case 19://
				regs[18] = (regs[18] & ~(1 << 3)) | ((command & 0b00000001)
						<< 3);
				SIDout(18, regs[18]);
				break;
			case 20://
				regs[18] = (regs[18] & ~(1 << 4)) | ((command & 0b00000001)
						<< 4);
				SIDout(18, regs[18]);
				break;
			case 21://
				regs[18] = (regs[18] & ~(1 << 5)) | ((command & 0b00000001)
						<< 5);
				SIDout(18, regs[18]);
				break;
			case 22://
				regs[18] = (regs[18] & ~(1 << 6)) | ((command & 0b00000001)
						<< 6);
				SIDout(18, regs[18]);
				break;
			case 23://
				regs[18] = (regs[18] & ~(1 << 7)) | ((command & 0b00000001)
						<< 7);
				SIDout(18, regs[18]);
				break;
			case 24://
				regs[23] = (regs[23] & ~(1 << 0)) | ((command & 0b00000001)
						<< 0);
				SIDout(23, regs[23]);
				break;
			case 25://
				regs[23] = (regs[23] & ~(1 << 1)) | ((command & 0b00000001)
						<< 1);
				SIDout(23, regs[23]);
				break;
			case 26://
				regs[23] = (regs[23] & ~(1 << 2)) | ((command & 0b00000001)
						<< 2);
				SIDout(23, regs[23]);
				break;
			case 27://
				regs[23] = (regs[23] & ~(1 << 3)) | ((command & 0b00000001)
						<< 3);
				SIDout(23, regs[23]);
				break;
			case 28://
				regs[24] = (regs[24] & ~(1 << 4)) | ((command & 0b00000001)
						<< 4);
				SIDout(24, regs[24]);
				break;
			case 29://
				regs[24] = (regs[24] & ~(1 << 5)) | ((command & 0b00000001)
						<< 5);
				SIDout(24, regs[24]);
				break;
			case 30://
				regs[24] = (regs[24] & ~(1 << 6)) | ((command & 0b00000001)
						<< 6);
				SIDout(24, regs[24]);
				break;
			case 31://
				regs[24] = (regs[24] & ~(1 << 7)) | ((command & 0b00000001)
						<< 7);
				SIDout(24, regs[24]);
				break;
			default:
				break;

			}
			// */

		} else {
			if (command & 0b00000001) {
				//set 16 bit - lowbyte first, pwm:12bit, else:16bit (filterfreq adjusted)
				uint8_t data1 = getbyte();
				uint8_t data2 = getbyte();
				if (targetbit > 15) {
					data1 += 128;
					targetbit -= 16;
				}
				if (targetbit > 7) {
					data2 += 128;
					targetbit -= 8;
				}
				switch (targetbit) {
				case 0: //freq voice 1
					regs[0] = data1;
					regs[1] = data2;
					SIDout(0, data1);
					SIDout(1, data2);
					break;
				case 1: //freq voice 2
					regs[7] = data1;
					regs[8] = data2;
					SIDout(7, data1);
					SIDout(8, data2);

					break;
				case 2: //freq voice 3
					regs[14] = data1;
					regs[15] = data2;
					SIDout(14, data1);
					SIDout(15, data2);

					break;
				case 3://pwm voice 1
					regs[2] = data1;
					regs[3] = data2;
					SIDout(2, data1);
					SIDout(3, data2);
					break;
				case 4://pwm voice 2
					regs[9] = data1;
					regs[10] = data2;
					SIDout(9, data1);
					SIDout(10, data2);
					break;
				case 5://pwm voice 3
					regs[16] = data1;
					regs[17] = data2;
					SIDout(16, data1);
					SIDout(17, data2);
					break;
				case 6://freq filter
					regs[21] = data1 >> 5;
					regs[22] = data2;
					break;
				default:

					break;
				}
			} else {
				uint8_t data1 = getbyte() & 0b00001111;
				switch (targetbit) {
				case 0: //at 1
					regs[5] = (regs[5] & 0b00001111) + (data1 << 4);
					SIDout(5, regs[5]);
					break;
				case 1://de 1
					regs[5] = (regs[5] & 0b11110000) + (data1);
					SIDout(5, regs[5]);
					break;
				case 2: //su 1
					regs[6] = (regs[6] & 0b00001111) + (data1 << 4);
					SIDout(6, regs[6]);
					break;
				case 3://re 1
					regs[6] = (regs[6] & 0b11110000) + (data1);
					SIDout(6, regs[6]);
					break;
				case 4://at 2
					regs[12] = (regs[12] & 0b00001111) + (data1 << 4);
					SIDout(12, regs[12]);
					break;
				case 5://de 2
					regs[12] = (regs[12] & 0b11110000) + (data1);
					SIDout(12, regs[12]);
					break;
				case 6: //su 2
					regs[13] = (regs[13] & 0b00001111) + (data1 << 4);
					SIDout(13, regs[13]);
					break;
				case 7://re 2
					regs[13] = (regs[13] & 0b11110000) + (data1);
					SIDout(13, regs[13]);
					break;
				case 8://at 3
					regs[19] = (regs[19] & 0b00001111) + (data1 << 4);
					SIDout(19, regs[19]);
					break;
				case 9://de 3
					regs[19] = (regs[19] & 0b11110000) + (data1);
					SIDout(19, regs[19]);
					break;
				case 10: //su 3
					regs[20] = (regs[20] & 0b00001111) + (data1 << 4);
					SIDout(20, regs[20]);
					break;
				case 11://re 3
					regs[20] = (regs[20] & 0b11110000) + (data1);
					SIDout(20, regs[20]);
					break;
				case 12://resonanz
					regs[23] = (regs[23] & 0b00001111) + (data1 << 4);
					SIDout(23, regs[23]);
					break;
				case 13://volume
					regs[24] = (regs[24] & 0b11110000) + (data1);
					SIDout(24, regs[24]);
					break;

				default:
					break;
				}
				//set 4bit

			}
		}

	} while (1);
	return (0);
}
