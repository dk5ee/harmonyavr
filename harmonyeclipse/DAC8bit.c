/*
 * DAC8bit.c
 *
 */

#include "DAC8bit.h"
#include <avr/pgmspace.h>
#include "defines.h"
#include "memory.h"

volatile uint8_t wavetable_nr = 0;
volatile uint16_t wavetable_pos = 0;

volatile uint16_t dacfrequency;
volatile uint8_t dacmulti;
volatile uint16_t dacintcount;
volatile uint16_t dacwavecount;
volatile uint8_t dacwavepos;
volatile uint8_t dacwavetableplaying = 0;


const uint8_t sinetable[] PROGMEM = { 128, // sine 0
		131, // sine 1
		134, // sine 2
		137, // sine 3
		140, // sine 4
		143, // sine 5
		146, // sine 6
		149, // sine 7
		152, // sine 8
		155, // sine 9
		158, // sine 10
		162, // sine 11
		165, // sine 12
		167, // sine 13
		170, // sine 14
		173, // sine 15
		176, // sine 16
		179, // sine 17
		182, // sine 18
		185, // sine 19
		188, // sine 20
		190, // sine 21
		193, // sine 22
		196, // sine 23
		198, // sine 24
		201, // sine 25
		203, // sine 26
		206, // sine 27
		208, // sine 28
		211, // sine 29
		213, // sine 30
		215, // sine 31
		218, // sine 32
		220, // sine 33
		222, // sine 34
		224, // sine 35
		226, // sine 36
		228, // sine 37
		230, // sine 38
		232, // sine 39
		234, // sine 40
		235, // sine 41
		237, // sine 42
		238, // sine 43
		240, // sine 44
		241, // sine 45
		243, // sine 46
		244, // sine 47
		245, // sine 48
		246, // sine 49
		248, // sine 50
		249, // sine 51
		250, // sine 52
		250, // sine 53
		251, // sine 54
		252, // sine 55
		253, // sine 56
		253, // sine 57
		254, // sine 58
		254, // sine 59
		254, // sine 60
		255, // sine 61
		255, // sine 62
		255, // sine 63
		255, // sine 64
		255, // sine 65
		255, // sine 66
		255, // sine 67
		254, // sine 68
		254, // sine 69
		254, // sine 70
		253, // sine 71
		253, // sine 72
		252, // sine 73
		251, // sine 74
		250, // sine 75
		250, // sine 76
		249, // sine 77
		248, // sine 78
		246, // sine 79
		245, // sine 80
		244, // sine 81
		243, // sine 82
		241, // sine 83
		240, // sine 84
		238, // sine 85
		237, // sine 86
		235, // sine 87
		234, // sine 88
		232, // sine 89
		230, // sine 90
		228, // sine 91
		226, // sine 92
		224, // sine 93
		222, // sine 94
		220, // sine 95
		218, // sine 96
		215, // sine 97
		213, // sine 98
		211, // sine 99
		208, // sine 100
		206, // sine 101
		203, // sine 102
		201, // sine 103
		198, // sine 104
		196, // sine 105
		193, // sine 106
		190, // sine 107
		188, // sine 108
		185, // sine 109
		182, // sine 110
		179, // sine 111
		176, // sine 112
		173, // sine 113
		170, // sine 114
		167, // sine 115
		165, // sine 116
		162, // sine 117
		158, // sine 118
		155, // sine 119
		152, // sine 120
		149, // sine 121
		146, // sine 122
		143, // sine 123
		140, // sine 124
		137, // sine 125
		134, // sine 126
		131, // sine 127
		128, // sine 128
		124, // sine 129
		121, // sine 130
		118, // sine 131
		115, // sine 132
		112, // sine 133
		109, // sine 134
		106, // sine 135
		103, // sine 136
		100, // sine 137
		97, // sine 138
		93, // sine 139
		90, // sine 140
		88, // sine 141
		85, // sine 142
		82, // sine 143
		79, // sine 144
		76, // sine 145
		73, // sine 146
		70, // sine 147
		67, // sine 148
		65, // sine 149
		62, // sine 150
		59, // sine 151
		57, // sine 152
		54, // sine 153
		52, // sine 154
		49, // sine 155
		47, // sine 156
		44, // sine 157
		42, // sine 158
		40, // sine 159
		37, // sine 160
		35, // sine 161
		33, // sine 162
		31, // sine 163
		29, // sine 164
		27, // sine 165
		25, // sine 166
		23, // sine 167
		21, // sine 168
		20, // sine 169
		18, // sine 170
		17, // sine 171
		15, // sine 172
		14, // sine 173
		12, // sine 174
		11, // sine 175
		10, // sine 176
		9, // sine 177
		7, // sine 178
		6, // sine 179
		5, // sine 180
		5, // sine 181
		4, // sine 182
		3, // sine 183
		2, // sine 184
		2, // sine 185
		1, // sine 186
		1, // sine 187
		1, // sine 188
		0, // sine 189
		0, // sine 190
		0, // sine 191
		0, // sine 192
		0, // sine 193
		0, // sine 194
		0, // sine 195
		1, // sine 196
		1, // sine 197
		1, // sine 198
		2, // sine 199
		2, // sine 200
		3, // sine 201
		4, // sine 202
		5, // sine 203
		5, // sine 204
		6, // sine 205
		7, // sine 206
		9, // sine 207
		10, // sine 208
		11, // sine 209
		12, // sine 210
		14, // sine 211
		15, // sine 212
		17, // sine 213
		18, // sine 214
		20, // sine 215
		21, // sine 216
		23, // sine 217
		25, // sine 218
		27, // sine 219
		29, // sine 220
		31, // sine 221
		33, // sine 222
		35, // sine 223
		37, // sine 224
		40, // sine 225
		42, // sine 226
		44, // sine 227
		47, // sine 228
		49, // sine 229
		52, // sine 230
		54, // sine 231
		57, // sine 232
		59, // sine 233
		62, // sine 234
		65, // sine 235
		67, // sine 236
		70, // sine 237
		73, // sine 238
		76, // sine 239
		79, // sine 240
		82, // sine 241
		85, // sine 242
		88, // sine 243
		90, // sine 244
		93, // sine 245
		97, // sine 246
		100, // sine 247
		103, // sine 248
		106, // sine 249
		109, // sine 250
		112, // sine 251
		115, // sine 252
		118, // sine 253
		121, // sine 254
		124, // sine 255
		126, // triangle 0
		128, // triangle 1
		129, // triangle 2
		131, // triangle 3
		133, // triangle 4
		135, // triangle 5
		137, // triangle 6
		139, // triangle 7
		141, // triangle 8
		143, // triangle 9
		145, // triangle 10
		147, // triangle 11
		149, // triangle 12
		151, // triangle 13
		153, // triangle 14
		155, // triangle 15
		157, // triangle 16
		159, // triangle 17
		161, // triangle 18
		163, // triangle 19
		165, // triangle 20
		167, // triangle 21
		169, // triangle 22
		171, // triangle 23
		173, // triangle 24
		175, // triangle 25
		177, // triangle 26
		179, // triangle 27
		181, // triangle 28
		183, // triangle 29
		185, // triangle 30
		187, // triangle 31
		189, // triangle 32
		191, // triangle 33
		193, // triangle 34
		195, // triangle 35
		197, // triangle 36
		199, // triangle 37
		201, // triangle 38
		203, // triangle 39
		205, // triangle 40
		207, // triangle 41
		209, // triangle 42
		211, // triangle 43
		213, // triangle 44
		215, // triangle 45
		217, // triangle 46
		219, // triangle 47
		221, // triangle 48
		223, // triangle 49
		225, // triangle 50
		227, // triangle 51
		229, // triangle 52
		231, // triangle 53
		233, // triangle 54
		235, // triangle 55
		237, // triangle 56
		239, // triangle 57
		241, // triangle 58
		243, // triangle 59
		245, // triangle 60
		247, // triangle 61
		249, // triangle 62
		251, // triangle 63
		253, // triangle 64
		255, // triangle 65
		253, // triangle 66
		251, // triangle 67
		249, // triangle 68
		247, // triangle 69
		245, // triangle 70
		243, // triangle 71
		241, // triangle 72
		239, // triangle 73
		237, // triangle 74
		235, // triangle 75
		233, // triangle 76
		231, // triangle 77
		229, // triangle 78
		227, // triangle 79
		225, // triangle 80
		223, // triangle 81
		221, // triangle 82
		219, // triangle 83
		217, // triangle 84
		215, // triangle 85
		213, // triangle 86
		211, // triangle 87
		209, // triangle 88
		207, // triangle 89
		205, // triangle 90
		203, // triangle 91
		201, // triangle 92
		199, // triangle 93
		197, // triangle 94
		195, // triangle 95
		193, // triangle 96
		191, // triangle 97
		189, // triangle 98
		187, // triangle 99
		185, // triangle 100
		183, // triangle 101
		181, // triangle 102
		179, // triangle 103
		177, // triangle 104
		175, // triangle 105
		173, // triangle 106
		171, // triangle 107
		169, // triangle 108
		167, // triangle 109
		165, // triangle 110
		163, // triangle 111
		161, // triangle 112
		159, // triangle 113
		157, // triangle 114
		155, // triangle 115
		153, // triangle 116
		151, // triangle 117
		149, // triangle 118
		147, // triangle 119
		145, // triangle 120
		143, // triangle 121
		141, // triangle 122
		139, // triangle 123
		137, // triangle 124
		135, // triangle 125
		133, // triangle 126
		131, // triangle 127
		129, // triangle 128
		128, // triangle 129
		126, // triangle 130
		124, // triangle 131
		122, // triangle 132
		120, // triangle 133
		118, // triangle 134
		116, // triangle 135
		114, // triangle 136
		112, // triangle 137
		110, // triangle 138
		108, // triangle 139
		106, // triangle 140
		104, // triangle 141
		102, // triangle 142
		100, // triangle 143
		98, // triangle 144
		96, // triangle 145
		94, // triangle 146
		92, // triangle 147
		90, // triangle 148
		88, // triangle 149
		86, // triangle 150
		84, // triangle 151
		82, // triangle 152
		80, // triangle 153
		78, // triangle 154
		76, // triangle 155
		74, // triangle 156
		72, // triangle 157
		70, // triangle 158
		68, // triangle 159
		66, // triangle 160
		64, // triangle 161
		62, // triangle 162
		60, // triangle 163
		58, // triangle 164
		56, // triangle 165
		54, // triangle 166
		52, // triangle 167
		50, // triangle 168
		48, // triangle 169
		46, // triangle 170
		44, // triangle 171
		42, // triangle 172
		40, // triangle 173
		38, // triangle 174
		36, // triangle 175
		34, // triangle 176
		32, // triangle 177
		30, // triangle 178
		28, // triangle 179
		26, // triangle 180
		24, // triangle 181
		22, // triangle 182
		20, // triangle 183
		18, // triangle 184
		16, // triangle 185
		14, // triangle 186
		12, // triangle 187
		10, // triangle 188
		8, // triangle 189
		6, // triangle 190
		4, // triangle 191
		2, // triangle 192
		0, // triangle 193
		2, // triangle 194
		4, // triangle 195
		6, // triangle 196
		8, // triangle 197
		10, // triangle 198
		12, // triangle 199
		14, // triangle 200
		16, // triangle 201
		18, // triangle 202
		20, // triangle 203
		22, // triangle 204
		24, // triangle 205
		26, // triangle 206
		28, // triangle 207
		30, // triangle 208
		32, // triangle 209
		34, // triangle 210
		36, // triangle 211
		38, // triangle 212
		40, // triangle 213
		42, // triangle 214
		44, // triangle 215
		46, // triangle 216
		48, // triangle 217
		50, // triangle 218
		52, // triangle 219
		54, // triangle 220
		56, // triangle 221
		58, // triangle 222
		60, // triangle 223
		62, // triangle 224
		64, // triangle 225
		66, // triangle 226
		68, // triangle 227
		70, // triangle 228
		72, // triangle 229
		74, // triangle 230
		76, // triangle 231
		78, // triangle 232
		80, // triangle 233
		82, // triangle 234
		84, // triangle 235
		86, // triangle 236
		88, // triangle 237
		90, // triangle 238
		92, // triangle 239
		94, // triangle 240
		96, // triangle 241
		98, // triangle 242
		100, // triangle 243
		102, // triangle 244
		104, // triangle 245
		106, // triangle 246
		108, // triangle 247
		110, // triangle 248
		112, // triangle 249
		114, // triangle 250
		116, // triangle 251
		118, // triangle 252
		120, // triangle 253
		122, // triangle 254
		124, // triangle 255
		};

/* todo: lots of things can be optimized here */
uint8_t getsine(uint8_t val) {
	return pgm_read_word(&(sinetable[val]));
}
uint8_t getsaw(uint8_t val) {
	return pgm_read_word(&(sinetable[256+val]));
}
uint8_t gettriangle(uint8_t val) {
	return pgm_read_word(&(sinetable[val+256]));
}
uint8_t getsquare(uint8_t val) {
	return (val > 127 ? 255 : 0);
}

void dac_loadwave(uint8_t wave, uint8_t wavtablenum) {
	if (wavtablenum < RAMWAVETABLES) {
		int counter = 0;
		switch (wave) {
		case 1:
			//sawtooth
			for (counter = 0; counter < 256; counter++) {
				wavetableram[wavtablenum][counter] = getsaw(counter);
			}
			break;
		case 2:
			//triangle
			for (counter = 0; counter < 256; counter++) {
				wavetableram[wavtablenum][counter] = gettriangle(counter);
			}
			break;
		case 3:
			//square
			for (counter = 0; counter < 256; counter++) {
				wavetableram[wavtablenum][counter] = getsquare(counter);
			}
			break;
		default:
			//sine
			for (counter = 0; counter < 256; counter++) {
				wavetableram[wavtablenum][counter] = getsine(counter);
			}
			break;
		}
		wavetablesize[wavtablenum] = 256;
		wavetablenext[wavtablenum] = wavtablenum; //looping;
	}
}

uint16_t dac_getintcount() {
	return dacintcount;
}

void dac_setfreq(uint16_t freq) {
	//silence at 9900..
	if (freq >= 9900) {
		freq = 0;
	}
	dacfrequency = freq;
}
void dac_setmulti(uint8_t multi) {
	if (multi > 127) {
		multi = 127;
	}
	dacmulti = multi;
}

void dac_clearwavetables() {
	for (uint8_t i = 0; i < RAMWAVETABLES; i++) {
		wavetablenext[i] = RAMWAVETABLES;
		wavetablesize[i] = 0;
		for (uint16_t j = 0; j < SINGLESAMPLESIZE; j++) {
			wavetableram[i][j] = 0;
		}
	}
}

void dac_playsample(uint8_t wavenum) {
	if (wavenum < RAMWAVETABLES) {
		dacwavepos = 0;
		dacwavetableplaying = wavenum;
	}
}

void dac_init() {
	dac_clearwavetables();

	dacfrequency = 0;
	dacmulti = 0;
	dacintcount = 0; //counts every interrupt
	dacwavecount = 0; //bresenham counter
	dacwavepos = 0;
	dacwavetableplaying = 3;
	// we use 16-bit Timer/Counter1 with CTC and 1000
	OCR1A = 1000;
	TCCR1A = 0;
	TCCR1B = 0;
	TCCR1B |= 1 << WGM12; //CTC
	TCCR1B |= 1 << CS10; //prescaler 1
	TIMSK1 |= 1 << OCIE1A; //
	DACDDRInit;
	dac_setfreq(440);
	dac_setmulti(0);
	dac_loadwave(0, 0);
}

ISR(TIMER1_COMPA_vect) {
	/*
	 *	this ISR calculates DAC value
	 *	previously 8bit int was tried, but in worst case this ISR would take too long
	 *	if this ISR is fired with 20kHz, it will take 25% of all cpu cycles.
	 */
	dacintcount++;
	if (dacwavetableplaying < RAMWAVETABLES) {
		uint8_t dwt = dacwavetableplaying;
		uint16_t dwsize = wavetablesize[dwt];
		uint16_t dwc = dacwavecount + dacfrequency;
		uint16_t dwp = dacwavepos;
		if (dwc > 5000) {
			dwc -= 5000;
			dwp += 1 << 6;
		}
		if (dwc > 2500) {
			dwc -= 2500;
			dwp += 1 << 5;
		}
		if (dwc > 1250) {
			dwc -= 1250;
			dwp += 1 << 4;
		}
		if (dwc > 625) {
			dwc -= 625;
			dwp += 1 << 3;
		}
		if (dwc > 312) {
			dwc -= 312;
			dwp += 1 << 2;
		}
		if (dwc > 156) {
			dwc -= 156;
			dwp += 1 << 1;
		}
		if (dwc > 78) {
			dwc -= 78;
			dwp += 1 << 0;
		}

		if (dwp >= dwsize) {
			dwp -= dwsize;
			dwt = wavetablenext[dwt];
			dacwavetableplaying = dwt;
			if (dwt >= RAMWAVETABLES) {
				return; //end of sample reached
			}
		}
		uint8_t value = wavetableram[dwt][dwp];
		uint8_t highvalue = (value * dacmulti + 32768) >> 8;
		DACport = highvalue;
		//	DACport = value;
		dacwavecount = dwc;
		dacwavepos = dwp;
	}
}
