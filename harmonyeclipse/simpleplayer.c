/*
 * simpleplayer.c
 *
 */
#include "simpleplayer.h"
const uint8_t spura[] = {
255 ,12,
8,4,
1,4,
5,3,
5,1,
8,4,
13,3,
13,1,
10,4,
8,3,
8,1,
5,4,
1,3,
3,1,
5,4,
3,4,
255,4,
1,3,
3,1,
5,4,
3,4,
255,4,
8,4,
1,4,
5,3,
5,1,
8,4,
13,3,
13,1,
10,4,
8,3,
8,1,
5,4,
1,3,
3,1,
5,4,
3,4,
5,4,
3,4,
1,12,
255,4,
3,8,
5,4,
8,4,
13,12,
12,4,
10,6,
12,2,
13,4,
10,4,
8,12,
13,2,
13,2,
10,4,
10,3,
10,1,
10,6,
10,2,
10,4,
8,4,
5,4,
1,3,
3,1,
5,6,
8,2,
8,6,
5,2,
3,4,
1,4,
255,8,
3,8,
5,4,
8,4,
13,12,
12,4,
10,6,
12,2,
13,4,
10,4,
8,12,
13,2,
13,2,
10,4,
10,3,
10,1,
10,6,
10,2,
10,4,
8,4,
5,4,
1,3,
3,1,
5,6,
8,2,
8,6,
5,2,
3,4,
1,4,
255,8,
255,255,
255,255};
const uint8_t spurb[] = {
255,12,
8,4,
1,4,
1,3,
1,1,
5,4,
5,3,
5,1,
6,4,
5,3,
5,1,
1,4,
1,3,
0,1,
1,4,
0,4,
255,4,
1,3,
0,1,
1,4,
0,4,
255,4,
0,4,
1,4,
1,3,
1,1,
5,4,
5,3,
5,1,
6,4,
5,3,
5,1,
5,4,
1,3,
0,1,
1,4,
0,4,
1,4,
0,4,
1,12,
255,4,
1,8,
1,4,
5,4,
5,12,
8,4,
6,6,
8,2,
10,4,
6,4,
5,12,
5,2,
5,2,
6,4,
6,3,
6,1,
6,6,
6,2,
6,4,
5,4,
1,4,
1,3,
0,1,
1,6,
5,2,
5,6,
1,2,
0,4,
1,4,
255,8,
1,8,
1,4,
5,4,
5,12,
8,4,
6,6,
8,2,
10,4,
6,4,
5,12,
5,2,
5,2,
6,4,
6,3,
6,1,
6,6,
6,2,
6,4,
5,4,
1,4,
1,3,
0,1,
1,6,
5,2,
5,6,
1,2,
0,4,
1,4,
255,8,
255,255,
255,255};
const uint8_t spurc[] = {
255,16,
1,16,
6,8,
1,8,
8,12,
1,4,
8,16,
1,16,
6,8,
1,8,
8,16,
1,16,
1,16,
1,16,
6,16,
1,16,
6,16,
1,16,
1,8,
5,8,
8,4,
8,4,
255,8,
1,16,
1,16,
6,16,
1,16,
6,16,
1,16,
1,8,
5,8,
8,4,
8,4,
255,8,
255,255,
255,255};

uint8_t spurenaktiv = 7;
uint8_t spuranow = 0;
uint8_t spurbnow = 0;
uint8_t spurcnow = 0;
uint8_t spuradur = 0;
uint8_t spurbdur = 0;
uint8_t spurcdur = 0;
uint8_t spuradelta = 42;
uint8_t spurbdelta = 42;
uint8_t spurcdelta = 30;
void initsteps() {
	spurenaktiv = 7;
	spuranow = 0;
	spurbnow = 0;
	spurcnow = 0;
	spuradur = 0;
	spurbdur = 0;
	spurcdur = 0;
	spuradelta = 42;
	spurbdelta = 42+12;
	spurcdelta = 30;
}
uint8_t rounda =0;
void nextstep() {
	if (rounda==255) {
		sidsetvolume(0);
		sidsetgate(2, 0);
		sidsetgate(1, 0);
		sidsetgate(0, 0);
		return;
	}
	if (spurenaktiv) {
		if (spuradur == 0 && (spurenaktiv & 1)) {
			sidsetgate(0, 0);
			uint8_t notea = spura[spuranow * 2];
			uint8_t dura = spura[spuranow * 2 + 1];
			if (dura < 255) {
				spuradur = dura;
				if (notea < 255) {
					notea = notea + spuradelta;
					sidsetnote(0, notea);
					sidsetgate(0, 1);
				} else {
					//pause
					sidsetgate(0, 0);
				}
			} else {
				//trackende this voice
				sidsetgate(0, 0);
				spurenaktiv &= ~1;
			}
			spuranow ++;
		}
		spuradur --;


		if (spurbdur == 0 && (spurenaktiv & 2)) {
			sidsetgate(1, 0);
			uint8_t noteb = spurb[spurbnow * 2];
			uint8_t durb = spurb[spurbnow * 2 + 1];
			if (durb < 255) {

				spurbdur = durb;
				if (noteb < 255) {
					noteb = noteb + spurbdelta;
					sidsetnote(1, noteb);
					sidsetgate(1, 1);
				} else {
					//pause
					sidsetgate(1, 0);
				}
			} else {
				//trackende this voice
				sidsetgate(1, 0);
				spurenaktiv &= ~2;
			}
			spurbnow ++;
		}
		spurbdur --;

		if (spurcdur == 0 && (spurenaktiv & 4)) {
			sidsetgate(2, 0);
			uint8_t notec = spurc[spurcnow * 2];
			uint8_t durc = spurc[spurcnow * 2 + 1];
			if (durc < 255) {
				spurcdur = durc;
				if (notec < 255) {
					notec = notec + spurcdelta;
					sidsetnote(2, notec);
					sidsetgate(2, 1);
				} else {
					//pause
					sidsetgate(2, 0);
				}
			} else {
				//trackende this voice
				sidsetgate(2, 0);
				spurenaktiv &= ~4;
			}
			spurcnow ++;
		}
		spurcdur --;

	} else {
		//end of all note, repeat
		sidloadinstrument(0,rounda);
		sidloadinstrument(1,rounda);
		sidloadinstrument(2,rounda);//5, 13
		rounda++;
		if (rounda>19) rounda =255;

		initsteps();
	}
}
