/*
 * ringuart0.h
 * 31250 baud uart for midi
 *
 */

#ifndef RINGUART0_H_
#define RINGUART0_H_
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/twi.h>
#include <avr/pgmspace.h>

void ringuart0init();
void tx0ring_enqueue(uint8_t data); // - am ende einfügen. automatisch senden starten.
uint8_t tx0ring_count(); // - noch zu sendende zeichen im buffer
uint8_t tx0ring_space(); // - noch verfügbarer space im buffer
uint8_t rx0ring_count(); // - empfangene zeichen im buffer
uint8_t rx0ring_space(); // - noch verfügbarer space im buffer
uint8_t rx0ring_dequeue(); // - erstes zeichen entfernen und ausliefern
uint8_t rx0ring_peek(uint8_t offset); // - kommendes zeichen entfernen
uint8_t rx0status();
uint8_t rx0resetstatus();

#endif /* RINGUART0_H_ */
