/*
 * ringuart1.c
 */
#include "ringuart1.h"
#include "defines.h"

#if defined (__AVR_ATmega644P__)
#else
#include <avr/iom644p.h>
#define	__AVR_ATmega644P__
#endif

//bitsize should be between 2 and 7
#define RINGUART1_RX1_BUFFER_BITSIZE 7
#define RINGUART1_TX1_BUFFER_BITSIZE 7
#define RX1RING_BUFFER (1<<RINGUART1_RX1_BUFFER_BITSIZE)
#define RX1RING_WRAP (RX1RING_BUFFER -1 )
#define TX1RING_BUFFER (1<<RINGUART1_TX1_BUFFER_BITSIZE)
#define TX1RING_WRAP (TX1RING_BUFFER -1 )

#define my1U2X U2X1
#define my1UBRRL UBRR1L
#define my1UBRRH UBRR1H
#define my1UCSRA UCSR1A
#define my1UCSRB UCSR1B
#define my1UCSRC UCSR1C
#define my1UCSZ0 UCSZ10
#define my1UCSZ1 UCSZ11
#define my1UDR UDR1
#define my1UDRIE UDRIE1
#define my1RX1CIE RXCIE1
#define my1RX1EN RXEN1
#define my1TX1CIE TXCIE1
#define my1TX1EN TXEN1

static uint8_t Rx1Ring[RX1RING_BUFFER];
static uint8_t Tx1Ring[TX1RING_BUFFER];
static volatile uint8_t Rx1Head = 0;
static volatile uint8_t Rx1Tail = 0;
static volatile uint8_t Rx1Status = 0;
static volatile uint8_t Tx1Head = 0;
static volatile uint8_t Tx1Tail = 0;

static void uart1_9600(void) {
#if defined BAUD
#undef BAUD
#endif
#define BAUD 9600
#include <util/setbaud.h>
	my1UBRRL = UBRRL_VALUE;
	my1UBRRH = UBRRH_VALUE;
	my1UCSRB = ((1 << my1RX1CIE) | (1 << my1RX1EN) | (1 << my1TX1EN));
	my1UCSRC = (1 << my1UCSZ1) | (1 << my1UCSZ0);
#if USE_2X
	my1UCSRA |= (1 << my1U2X);
#else
	my1UCSRA &= ~(1 << my1U2X);
#endif
}

void ringuart1init() {
	cli();
	uart1_9600();
	sei();
}
void tx1ring_enqueue(uint8_t data) {
	//wait while tx1ring is full..
	while ((((Tx1Tail + 1) - Tx1Head) & TX1RING_WRAP) == 0) {
		//do nothing
		// TODO: idle function?
	}
	uint8_t tempadr = (Tx1Tail + 1) & TX1RING_WRAP;
	Tx1Ring[tempadr] = data;
	Tx1Tail = tempadr;
	//now: always activate interrupt for sending
	my1UCSRB |= (1 << my1UDRIE);
}
uint8_t tx1ring_count() {
	return ((Tx1Tail - Tx1Head) & TX1RING_WRAP);
}
uint8_t tx1ring_space() {
	return (TX1RING_WRAP - ((Tx1Tail - Tx1Head) & TX1RING_WRAP));
}
uint8_t rx1ring_count() {
	return ((Rx1Tail - Rx1Head) & RX1RING_WRAP);
}
uint8_t rx1ring_space() {
	return (RX1RING_WRAP - ((Rx1Tail - Rx1Head) & RX1RING_WRAP));
}
uint8_t rx1ring_dequeue() {
	if (Rx1Tail == Rx1Head) {
		return 255;
	} else {
		uint8_t tempadr = (Rx1Head + 1) & RX1RING_WRAP;
		Rx1Head = tempadr;
		return Rx1Ring[tempadr];
	}
}
uint8_t rx1ring_peek(uint8_t offset) {
	return Rx1Ring[(1 + offset + Rx1Head) & RX1RING_WRAP];
}

/*void ring_outbyte(uint8_t data) {
 tx1ring_enqueue('0');
 tx1ring_enqueue('x');
 tx1ring_enqueue(hex[(data >> 4)]);
 tx1ring_enqueue(hex[data & 15]);
 }*/
inline void ring_outchar(uint8_t data) {
	//wrapper
	tx1ring_enqueue(data);
}
void senddownkey(uint8_t key){
	tx1ring_enqueue(0xAA);
	tx1ring_enqueue(key);
}
void sendupkey(uint8_t key){
	tx1ring_enqueue(0xAB);
	tx1ring_enqueue(key);
}
void senddownSkey(uint8_t key){
	tx1ring_enqueue(0xAC);
	tx1ring_enqueue(key);
}
void sendupSkey(uint8_t key){
	tx1ring_enqueue(0xAD);
	tx1ring_enqueue(key);
}
void sendkey(uint8_t key, uint8_t down) {
	if (down) {
		senddownkey(key);
	} else {
		sendupkey(key);
	}
}
void sendSkey(uint8_t key, uint8_t down){
	if (down) {
		senddownSkey(key);
	} else {
		sendupSkey(key);
	}
}

ISR ( USART1_RX_vect ) {
	uint8_t data;
	data = my1UDR;
	uint8_t temp;
	temp = ((Rx1Tail + 1) & RX1RING_WRAP);
	if (temp == Rx1Head) {
		Rx1Status |= 4; //overflow happened.. OMG!
	} else {
		Rx1Tail = temp;
		Rx1Ring[temp] = data;
	}
}
ISR(USART1_UDRE_vect) {
	if ((Tx1Tail - Tx1Head) & TX1RING_WRAP) {
		uint8_t temp;
		temp = ((Tx1Head + 1) & TX1RING_WRAP);
		Tx1Head = temp;
		my1UDR = Tx1Ring[temp];
	} else {
		my1UCSRB &= ~(1 << my1UDRIE); //disable sending interrupt
	}
}

// unused
EMPTY_INTERRUPT(USART1_TX_vect);

