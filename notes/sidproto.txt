befehle:
bits:
0	steuerbit 1=tonan
1	sync
2	ringmodulation
3	reinit noise
4	dreieck
5	rechteck
6	sägezahn
7	rauschen
8	
9
10
11
12
13
14
15	--
16
17
18
19
20
21
22
23	--
24	voice1 über filter
25	voice2 über filter
26	voice3 über filter
27	extern über filter
28	tiefpass
29	bandpass
30	hochpass
31	voice 3 mute
4bits:
0	A
1	D
2	S
3	R
4	A
5	D
6	S
7	R
8	A
9	D	
10	S
11	R
12	resonanz
13	volume
16bits:
0	freq1	16bit
1	freq2	16bit
2	freq3	16bit
3	pwm1	12bit
4	pwm2	12bit	
5	pwm3	12bit
6	filterfreq - 16bit >>5


=====
ziel: daten übertragen von arduino auf sid.

max. 3x7 bit datenpakete

daten die anfallen

die längsten daten:
7*16 bit daten
	3*16 bit frequenz 3 stimmen
	3*12 bit pwm	3 stimmen
	1*11 bit filterfrequenz
	3 bit addy + 16 bit daten= 19 bit
alternativ:
3*16 bit frequenz 3 stimmen
	2 bit addy + 16 bit daten= 18 bit

4*14 bit pwm und filterfrequenz
	2 bit addy + 14 bit daten= 16 bit

standard daten:
25*8 bit register direkt setzen
	5 bit adresse + 8bit daten = 12 bit

kurze daten:
14*4 bit werte
	3*4 ADSR
	1* filterresonanz
	1* volume

	4 bit adresse + 4 bit daten = 8 bit

32*1 bit
	3*4 bit waveform select 
	3*4 bit sys (modul, noisereset, enable, sync)
	1*4 bit filter enable
	1*1 bit voice 3 mute
	3*1 bit filter pass
	
	5 bit adresse + 1bit on/off


	6	5	4	3	2	1	0
	|-addy				--|	0	1	set bit
	|-addy				--|	0	0	unset bit
	0	|-addy			--|	1	0	set 4bit	- ein datenpaket folgt mit 4bit wert
	hibit2	hibit1	|-addy		--|	1	1	set 16 bit	- zweimal 7bit paket folgt

	nachher zb noch moglich:
	1	0	0	|-addy	--|	1	0	set tone	- ein datenpaket folgt mit 7bit wert - wird zu frequenz umgerechnet
	1	0	1	|-addy	--|	1	0	play tone 	- wie set tone, wird sofort gespielt
	1	1	0	|-addy	--|	1	0	set instrument	- instrument wird eingestellt - 7bit wert
	1	1	1	|-addy	--|	1	0	effekte?


;SID-ADR-Table:
;
;     VALUE    ATTACK    DECAY/RELEASE
;   +-------+----------+---------------+
;   |   0   |    2 ms  |      6 ms     |
;   |   1   |    8 ms  |     24 ms     |
;   |   2   |   16 ms  |     48 ms     |
;   |   3   |   24 ms  |     72 ms     |
;   |   4   |   38 ms  |    114 ms     |
;   |   5   |   56 ms  |    168 ms     |
;   |   6   |   68 ms  |    204 ms     |
;   |   7   |   80 ms  |    240 ms     |
;   |   8   |  100 ms  |    300 ms     |
;   |   9   |  240 ms  |    720 ms     |
;   |   A   |  500 ms  |    1.5 s      |
;   |   B   |  800 ms  |    2.4 s      |
;   |   C   |    1 s   |      3 s      |
;   |   D   |    3 s   |      9 s      |
;   |   E   |    5 s   |     15 s      |
;   |   F   |    8 s   |     24 s      |
;   +-------+----------+---------------+	
FREQUENCY = (REGISTER VALUE * 5.8) + 30 Hz --- filter
FREQUENCY = (REGISTER VALUE * CLOCK)/16777216 Hz
	where CLOCK=1022730 for NTSC systems and CLOCK=985250 for PAL systems, and 1000000 here.

The possibility to control these things for each voice (usually, many of
  these features are stored within the instrument (sound) data.)
  * ADSR envelope
  * Pulsewidth, and modulating it (changing smoothly)
  * Vibrato (modulating the frequency of a note back and forth)
  * Sliding (smooth change of frequency, either up or down)
  * Waveforms and arpeggios. Usually combined under a single table
  * Hard restart. A few (usually two) frames before the start of a new note
    the gatebit is cleared and attack/decay + sustain/release are set
    to a preset value (usually 0), to make sure the attack of the next
    note starts reliably. In an advanced routine, this can be turned on
    and off at will, or several different methods of hardrestart can
    be chosen.
  * Tying notes to each other, that is just to change the frequency but to not
    clear the gatebit, perform hardrestart or re-init pulsewidth. This can be
    used to simulate guitar riffs using pull-off and hammer-on techniques...
The usual flow of musicroutine execution is:
- Process one frame of 1st voice
- Process one frame of 2nd voice
- Process one frame of 3rd voice
- Process one frame of non-voice specific things (filter!)

effekte:
For echo-like effects, there could also be the
possibility to modify the sustain/release register in the middle of a note with
a pattern data command.

As the note plays on, the pulsewidth can then be changed (modulated) for
interesting effects. There are many ways to assign the parameters for this, one
way is:
Initial pulsewidth
Pulsewidth modulation speed
Pulsewidth limit low
Pulsewidth limit high

vibrato:
Speed of vibrato (how much the frequency is changed each frame)
Width of vibrato (how many frames before changing direction)

arpeggio: broken accord

slide: toneportamento
Slide speed (how much, and to what direction the frequency changes each frame)
The duration of the slide

complex waveform/arpeggio effect (like drumsounds)

waveform 0 used to indicate the waveform doesn't change

To execute the hard restart, clear the waveform register's gate bit and
set ADSR registers to 0 a couple of frames before the note's end 
When starting a new note after a hard restart, the registers should be
written to in this order: Waveform - Attack/Decay - Sustain/Release.

Tied notes:
Simple: don't reset gatebit, don't do hardrestart, don't reset pulsewidth, just
change the frequency.

Drums are made from a noise channel and also a fast frequency down, with fast decay.
Bass drums use a square wave, and only the first 50th of a second is a noise channel.
Hihats and other drums use noise all the time
'skydive'. This is a slower frequency down
octave arpeggio.




instrument
inst 0-hubbass           inst 1-crash             inst 2-crish-v1      inst 3=cridh-v2
atdk=09                  atdk=0a                  atdk=0d              atdk=0f
surl=d0                  surl=09                  surl=f1              surl=b4
pwid=0102 01ff 0016      freq=0f82 1f04           creg=15 81 81 15     pwid=0200
creg=41                  creg=81 80               gate=14              creg=43 81 81 43
gate=40                  gate=88                                       gate=42
                                                                       inst=9-harmo2
inst=4-hubbarmo          inst=5-bublead           inst=6-beop
                                                                       atdk=06
atdk=06                  atdk=29                  atdk=0a
                                                                       surl=c9
                         surl=99                  surl=0800
surl=ea
                                                                       pwid=0800 0006
                         pwid=0900 09a8 0a50 0af8 freq=0dd0 0100 0100
pwid=01800
                                                                       creg=41 81 81 40
                         0ba0 0c48                creg=41 81 81 40
creg=41 81 81 41
                                                  gate=40
gate=40                  creg=41
                         frq+=0000 0040 0080 00c0
                         00c0 0080
                         gate=40
                                                  inst=scream          inst f-hard
                         inst=b-scream+
inst=a-scream-
atdk=38                  atdk=29                  atdk=38
                                                  surl=7a
surl=9a                  surl=99
pwid=0b20 0c00 0ce0 0dc0 pwid=0900 09c0 0a80 0c00 pwid=0b20 0c00 0ce0
                              0cc0                      0dc0 0ea0 0dc0
     0ea0 0dc0
creg=41                  creg=41                  creg=41
gate=40                  frq+=0000 0028           frq+=0000 0094 0094
                                                       0094 0000 ff6c
                         gate=40


                -----------------  11bit  ------------
                |Cutoff freq reg|-------->|Cutoff D/A|---------o
                -----------------         ------------         |
                    $d415-16                                   |
                                                               |
                -----------------  4bit   ------------         |
                |Resonance reg. |-------->|Reson. D/A |-o      |
                -----------------         ------------- |      |
                    $d417.[4-7]                         |      |
                                                        |      |
                                 =0                     v      v
-----------    -----------     >o------------>|    ------------------
|wave D/A |--->|env. D/A |-->o/               |    |                |
-----------    -----------      o--->|        |    |                |
     ^              ^          ^ =1  o--------|--->|                |
     |12bit         |8bit      |     |        |    |                |
     |              |          |     |        |    |                |
-----------    -----------     |     |        |    |                |
|OSC1 +   |    |ADSR cnt+|     |     |        |    |                |
|wave sel.|    |env. log.|     |     |        |    |                |
-----------    -----------     |     |        |    |                |
$d400-03,      $d405-06,    $d417.0  |        |    |                |
$d404.[1-7]    $d404.0               |        |    |     FILTER     |
                                     |        |    |                |
                                 =0  |        |    |                |
-----------    -----------     >o----|------->|    |                |
|wave D/A |--->|env. D/A |-->o/      |        |    |                |
-----------    -----------      o--->|        |    |                |
     ^              ^          ^ =1  |        |    |                |
     |12bit         |8bit      |     |        |    |                |
     |              |          |     |        |    |                |
-----------    -----------     |     |        |    |                |
|OSC2 +   |    |ADSR cnt+|     |     |        |    |                |
|wave sel.|    |env. log.|     |     |        |    |                |
-----------    -----------     |     |        |    |                |
$d407-0a,      $d40c-0d,    $d417.1  |        |    |                |
$d40b.[1-7]    $d40b.0               |        |    | LP   BP   HP   |
                                     |     =0 |    ------------------
                                 =0  |    >o->|       |    |    |
-----------    -----------     >o----|--o/    |   *** o    o    o  
|wave D/A |--->|env. D/A |-->o/      |     o- |      /    /    /
-----------    -----------      o--->|  ^  =1 |   =0 V    V    V   =1
     ^              ^          ^ =1  |  |     |      o o  o o  o o
     |12bit         |8bit      |     |  |     |      | |  | |  | |
     |              |          |     |$d418.7 |<-------o    |    |
-----------    -----------     |     |        |<------------o    |
|OSC3 +   |    |ADSR cnt+|     |     |        |<-----------------o
|wave sel.|    |env. log.|     |     |        |
-----------    -----------     |     |        |
$d40e-11,      $d413-14,    $d417.2  |        |
$d412.[1-7]    $d412.0               |        |    -----------------
                                     |        |    | Master volume |AUDIO
                                 =0  |        o--->|     D/A       |----->
                               >o----|------->|    ----------------- OUT
EXT IN --------------------->o/      |                     ^
                                o--->|                     |4bit
                               ^ =1           ^            |
                               |              |       $d418.[0-3]
                            $d417.3  ^        |
                                     |        |
                    Analog mixing ---|--------|


