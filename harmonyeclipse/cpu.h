/*
 * cpu.h
 *
 *  Created on: 22 Jan 2020
 *  Source: https://github.com/cadaver/siddump/blob/master/cpu.h
 */

#ifndef CPU_H_
#define CPU_H_

#include <avr/io.h>

void init6502(uint16_t newpc, uint8_t newa, uint8_t newx, uint8_t newy);
uint8_t run6502();

#endif /* CPU_H_ */
