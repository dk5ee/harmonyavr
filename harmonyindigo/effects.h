/*
 * effects.h
 *
 * one "waitduration" should be about 1 ms
 * voices:
 * 0..2 : frequency of channel, voice1, voice2, voice3
 * 3..5 : PWM value for voice1, voice2, voice3
 * 6 : filter frequency
 * 7 : volume frequency
 */

#ifndef EFFECTS_H_
#define EFFECTS_H_
#include <avr/io.h>
#include "sidfunctions.h"

void playnote(uint8_t voice, uint8_t note);
void endnode(uint8_t voice);

//polyphone thingy
void playpolynote(uint8_t note);
void endpolynote(uint8_t note);

//polyphone only normal instruments without further effects
void setpolyphone(uint8_t instrument);

//single voice effects

/*
 * clear all effects
 */
void setinitvalue(uint8_t voice, uint8_t instrument);

/*
 * drum effect
 * first set noise waveform,
 * then switch to instruments after waitduration
 */
void setdrum(uint8_t voice, uint8_t instrument, uint16_t waitduration) ;
/*
 * arpeggio effect
 * cycle through notes, switch after each waitduration
 */
void setarpeggio(uint8_t voice, uint8_t note1, uint8_t note2, uint8_t note3,
		uint16_t waitduration);
/*
 * vibrato effect
 * raise and lower frequency by amount
 * one up and down cycle is two waitduration
 */
void setvibrato(uint8_t voice, uint16_t amount, uint16_t stepsperwait,
		uint8_t waits);
/*
 * glissando effect
 * change note by halftone step to targetnote, use waitduration for each step
 */
void setglissando(uint8_t voice, uint8_t targetnote, uint8_t waitduration);
/*
 * dive/climb to targetfrequency
 * after each waitduration alter frequency by stepamount
 */
void setdive(uint8_t voice, uint16_t targetfreq, int16_t stepamount,
		uint8_t stepsperwait);

/*
 * tick - should be called about every millisecond
 */

void dotick();

#endif /* EFFECTS_H_ */
