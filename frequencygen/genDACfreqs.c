/*

 compile with 
 gcc -lm genDACfreqs.c 

 */

#include <stdio.h>
#include <math.h>

#define PI 3.14159265
#define TAU (2.0*PI)

int main() {
	char notes[] = "AAHCCDDEFFGG";
	char notes2[] = " #  # #  # #";
	printf("const uint8_t sinetable[] PROGMEM = {\n");
	int i = 0;
	for (i = 0; i < 256; i++) {
		
		//calculate sine value
		double a = TAU*(((double)(i))/256.0);
		double b = ((sin(a)+1.0)/2.0)*255.0;
		
		int val = round(b);
		
		printf("\t%d, // sine %d \n", val, i);
	}
	for (i = 0; i < 256; i++) {
		
		//calculate saw value
		int j = (i + 63)%256;
		double a = 2.0*(((double)(j))/256.0);
		if (a>1.0) a= 2.0-a;
		double b = (a)*255.0;
		
		int val = round(b);
		
		printf("\t%d, // triangle %d \n", val, i);
	}
	printf("};\n\n");
	printf("\n /* todo: lots of things can be optimized here */\n");
	printf("uint8_t getsine(uint8_t val) {\n\treturn pgm_read_word(&(sinetable[val]));\n}\n");
	printf("uint8_t getsaw(uint8_t val) {\n\treturn pgm_read_word(&(sinetable[256+val]));\n}\n");
	printf("uint8_t gettriangle(uint8_t val) {\n\tval+=63;\n\treturn (val>127?val;\n}\n");
	printf("uint8_t getsquare(uint8_t val) {\n\treturn (val>127?255:0);\n}\n");
		
	return 0;
}