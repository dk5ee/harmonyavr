/*
 * memory.h
 *
 */

#ifndef MEMORY_H_
#define MEMORY_H_
#include <inttypes.h>
#include <avr/io.h>


#define SINGLESAMPLESIZE 4096
#define RAMWAVETABLES 3
#define MAXSAMPLESIZE (SINGLESAMPLESIZE * RAMWAVETABLES)
//for simpler sounds, three pages, 4kB, for DAC sound
uint8_t wavetableram[RAMWAVETABLES][SINGLESAMPLESIZE];
uint16_t wavetablepos[RAMWAVETABLES]; //holding size of this wavetable
uint16_t wavetablesize[RAMWAVETABLES]; //holding size of this wavetable
uint8_t wavetablenext[RAMWAVETABLES]; //next wavetable 0..2, '3' is end of sample
uint8_t c64zeropage_6502[256]; //0x0000.. 0x00FF
uint8_t c64stack_6502[256]; //0x0100.. 0x01FF
uint8_t c64top_6502[16]; //  //0xFFF0.. 0xFFFF

uint8_t readfakememory(uint16_t address);
void writefakememory(uint16_t address, uint8_t data);
//returns count of loaded values
uint16_t loadROMtoRAMsize(uint8_t ramnumber, long rompos, uint16_t targetpos, uint16_t length);
void loadROMtoRAMmax(long rompos, uint16_t targetpos, uint16_t length);


#endif /* MEMORY_H_ */
