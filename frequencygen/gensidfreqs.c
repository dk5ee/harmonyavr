/*

 compile with 
 gcc -lm gensidfreqs.c 

 */

#include <stdio.h>
#include <math.h>

#define Fclk  1000000.0
#define Midi_A_note_number 69
#define Midi_A_note_frequency 440.0
#define halftonestep pow(2.0, (1.0/12.0) )

int main() {
	char notes[] = "AAHCCDDEFFGG";
	char notes2[] = " #  # #  # #";
	printf("const uint16_t midinote2sid[] PROGMEM = {\n");
	int i = 0;
	for (i = 0; i < 128; i++) {
		//calculate frequency for midi note
		double f = pow(halftonestep,
				i - Midi_A_note_number)*Midi_A_note_frequency;
		//calculate SID register value for frequency
		int val = round(f * 16777216.0 / Fclk);
		if (val > 65535) {
			break;
		}
		char c = notes[(120+i - Midi_A_note_number)%12];
		char c2 = notes2[(120+i - Midi_A_note_number)%12];
		printf("\t%d, // Midi Note %c%c %d, %fHz \n", val, c, c2, i, f);
	}
	printf("};\nconst uint16_t midilastnote = %d;\n", i - 1);
	printf("uint16_t sidgetfreq(uint8_t midinote) {\n\treturn pgm_read_word(&(midinote2sid[midinote]));\n}\n");
	return 0;
}