/*
 * effects.c
 *
 */

#include "effects.h"
#include "sidfunctions.h"

/*
 * globalmode
 * 0 : polyphone
 */
uint8_t globalmode = 0;
typedef struct {
	uint8_t voicemode; //absolute or by note
	uint8_t notevalue;
	uint16_t frequencyvalue;
	uint16_t targetfrequency;
	uint16_t vibratoamount;
	uint16_t vibratostepsperwait;
	uint8_t vibratowaits;
	uint8_t vibratonow;
	int16_t vibratooffsetnow;
	uint8_t vibratodir;
	uint8_t targetnote;
	uint16_t targetwaits;
	uint16_t targetwaitsdone;
	int16_t targetstep;
} valuestruct;

typedef struct {
	/*
	 * bit zero: drum
	 * bit one: arpeggio
	 * bit two: vibrato
	 * bit tree: glissando
	 * bit for: dive/climb
	 */
	uint8_t singlemode;
	uint8_t instrument;
	uint16_t drumwaits;
	uint16_t drumnow;
	uint16_t arpeggiowaits;
	uint16_t arpeggionow;
	uint8_t arpeggionotes[3];
	uint8_t arpeggionote;
	uint8_t drumlatterwave;
} singleeffects;

singleeffects voiceeffects[3];
valuestruct values[7];
uint8_t polynotes[3];

void playnote(uint8_t voice, uint8_t note) {
	if (voice > 2)
		return;
	if (globalmode) {
		//check drum effect
		if (voiceeffects[voice].singlemode & 1 << 0) {
			sidsetwaveform(voice,4);
			voiceeffects[voice].drumnow = 0;
		}
	}
	values[voice].notevalue = note;
	uint16_t freq = sidgetfreq(note);
	values[voice].frequencyvalue = freq;
	sidsetfreq(voice, freq);
	sidsetgate(voice, 1);
	//start note
}
void endnode(uint8_t voice) {
	if (voice > 2)
		return;
	sidsetgate(voice, 0);
	//end note
}

//polyphone thingy
void playpolynote(uint8_t note) {
	if (note == 0)
		return;
	uint8_t counter = 0;
	for (counter = 0; counter < 3; counter++) {
		if (polynotes[counter] == 0) {
			polynotes[counter] = note;
			playnote(counter, note);
			return;
		}
	}
}
void endpolynote(uint8_t note) {
	if (note == 0)
		return;
	uint8_t counter = 0;
	for (counter = 0; counter < 3; counter++) {
		if (polynotes[counter] == note) {
			polynotes[counter] = 0;
			//endnote(counter);
			sidsetgate(counter, 0);
		}
	}
}

//polyphone only normal instruments without further effects
void setpolyphone(uint8_t instrument) {
	uint8_t counter = 0;
	for (counter = 0; counter < 3; counter++) {
		sidloadinstrument(counter, instrument);
		voiceeffects[counter].instrument = instrument;
		sidloadinstrument(counter, instrument);
		polynotes[counter] = 0; //zero = off
	}
	globalmode = 0;
}

//single voice effects
/*
 * clear all effects
 */
void setinitvalue(uint8_t voice, uint8_t instrument) {
	if (voice > 2)
		return;
	sidloadinstrument(voice, instrument);
	voiceeffects[voice].drumnow = 0;
	voiceeffects[voice].drumwaits = 0;
	voiceeffects[voice].instrument = instrument;
	sidloadinstrument(voice, instrument);
	voiceeffects[voice].singlemode = 0;
}

/*targetwaitsdone
 * drum effect
 * first set noise waveform,
 * then switch to instruments after waitduration
 */
void setdrum(uint8_t voice, uint8_t instrument, uint16_t waitduration) {
	globalmode = 1;
	voiceeffects[voice].drumnow = 0;
	voiceeffects[voice].drumwaits = waitduration;
	voiceeffects[voice].instrument = instrument;
	sidloadinstrument(voice, instrument);
	voiceeffects[voice].drumlatterwave = sidreadreg(voice * 7 + 4) >> 4;
	voiceeffects[voice].singlemode |= 1 << 0;
}
/*
 * arpeggio effect
 * cycle through notes, switch after each waitduration
 */
void setarpeggio(uint8_t voice, uint8_t note1, uint8_t note2, uint8_t note3,
		uint16_t waitduration) {
	globalmode = 1;
	voiceeffects[voice].singlemode |= 1 << 1;
	voiceeffects[voice].arpeggionotes[0] = note1;
	voiceeffects[voice].arpeggionotes[1] = note2;
	voiceeffects[voice].arpeggionotes[2] = note3;
	voiceeffects[voice].arpeggionote = 0;
	voiceeffects[voice].arpeggionow = 0;
	voiceeffects[voice].arpeggiowaits = waitduration;
}
/*
 * vibrato effect
 * raise and lower frequency by amount
 * one up and down cycle is two waitduration
 */
void setvibrato(uint8_t voice, uint16_t amount, uint16_t stepsperwait,
		uint8_t waits) {
	globalmode = 1;
	if (voice > 7)
		return;
	if (voice < 3) {
		voiceeffects[voice].singlemode |= 1 << 2;
	}
	if (amount > 0x7ffe) {
		//avoid clipping
		amount = 0x7ffe;
	}
	values[voice].vibratoamount = amount;
	values[voice].vibratodir = 0;
	values[voice].vibratostepsperwait = stepsperwait;
	values[voice].vibratowaits = waits;
	values[voice].vibratonow = 0;
}
/*
 * glissando effect
 * change note by halftone step to targetnote, use waitduration for each step
 */
void setglissando(uint8_t voice, uint8_t targetnote, uint8_t waitduration) {
	globalmode = 1;
	if (voice > 7)
		return;
	if (voice < 3) {
		voiceeffects[voice].singlemode |= 1 << 3;

	}
	values[voice].targetnote = targetnote;
	values[voice].targetfrequency = 0;
	values[voice].targetwaits = waitduration;
	values[voice].targetwaitsdone = 0;
	values[voice].targetstep = 0;
}
/*
 * dive/climb to targetfrequency
 * after each waitduration alter frequency by stepamount
 */
void setdive(uint8_t voice, uint16_t targetfreq, int16_t stepamount,
		uint8_t stepsperwait) {
	globalmode = 1;
	if (voice > 7)
		return;
	if (voice < 3) {
		voiceeffects[voice].singlemode |= 1 << 3;

	}
	values[voice].targetnote = 0;
	values[voice].targetfrequency = targetfreq;
	values[voice].targetwaits = stepsperwait;
	values[voice].targetstep = stepamount;
}

/*
 * tick - should be called about every millisecond
 */

void dotick() {
	uint8_t counter = 0;
	if (globalmode == 0) {
		//polyphonic mode, less effects
	} else {
		for (counter = 0; counter < 3; counter++) {
			uint8_t mode = voiceeffects[counter].singlemode;
			if (mode & 1 << 0) {
				//drum mode
				uint16_t drumnow = voiceeffects[counter].drumnow;
				uint16_t drumwaits = voiceeffects[counter].drumwaits;
				if (drumnow <= drumwaits) {
					if (drumnow == drumwaits) {
						sidsetwaveform(counter,
								voiceeffects[counter].drumlatterwave);
					}
					voiceeffects[counter].drumwaits++;
				}
			}
			if (mode & 1 << 1) {
				//arpeggio -cycle through 3 notes
				uint16_t arpeggionow = voiceeffects[counter].arpeggionow;
				uint16_t arpeggiowaits = voiceeffects[counter].arpeggiowaits;
				if (arpeggiowaits == arpeggionow) {
					//note change now
					uint8_t arpeggionote = voiceeffects[counter].arpeggionote;
					arpeggionote++;
					if (arpeggionote > 2)
						arpeggionote = 0;
					sidsetnote(counter,
							voiceeffects[counter].arpeggionotes[arpeggionote]);
					voiceeffects[counter].arpeggionote = arpeggionote;
					voiceeffects[counter].arpeggionow = 0;
				} else {
					voiceeffects[counter].arpeggionow = arpeggionow + 1;
				}

			}
			if (mode & 1 << 2) {
				uint8_t vibratowaits = values[counter].vibratowaits;
				uint8_t vibratonow = values[counter].vibratonow;
				if (vibratonow == vibratowaits) {
					values[counter].vibratonow = 0;
					uint8_t vibratodir = values[counter].vibratodir;
					int16_t vibratooffset = values[counter].vibratooffsetnow;
					uint16_t vibratostepsperwait =
							values[counter].vibratostepsperwait;
					uint16_t vibratoamount = values[counter].vibratoamount;
					uint16_t basefreq = values[counter].frequencyvalue;
					if (vibratodir == 0) {
						//up
						if (vibratoamount - vibratostepsperwait
								> vibratooffset) {
							vibratooffset += vibratostepsperwait;
						} else {
							vibratooffset = vibratoamount;
							values[counter].vibratodir = 1;
						}
					} else {
						if (vibratoamount + vibratooffset
								> vibratostepsperwait) {
							vibratooffset -= vibratostepsperwait;
						} else {
							vibratooffset = -vibratoamount;
							values[counter].vibratodir = 0;
						}
					}
					values[counter].vibratooffsetnow = vibratooffset;
					if (vibratooffset < 0) {
						if (-vibratooffset > basefreq) {
							//avoid clipping
							sidsetfreq(counter, 1);
						} else {
							sidsetfreq(counter, basefreq + vibratooffset);
						}
					} else {
						if (65535 - vibratooffset > basefreq) {
							//avoid clipping
							sidsetfreq(counter, 65535);
						} else {
							sidsetfreq(counter, basefreq + vibratooffset);
						}
					}
				} else {
					values[counter].vibratonow = vibratonow + 1;
				}
			}
			if (mode & 1 << 3) {
				//glissando
				uint8_t targetnote = values[counter].targetnote;
				uint8_t actualnote = values[counter].notevalue;
				if (targetnote != actualnote) {
					uint16_t targetwait= values[counter].targetwaits;
					uint16_t targetwaitdone = values[counter].targetwaitsdone;
					targetwaitdone++;
					if (targetwaitdone >= targetwait) {
						if (targetnote >actualnote) {
							actualnote ++;
						} else {
							actualnote --;
						}
						sidsetnote(counter,actualnote);
						values[counter].notevalue = actualnote;
						targetwaitdone = 0;
					}
					values[counter].targetwaitsdone = targetwaitdone;
				} else {
					//todo: signal glissando done?
				}
			}
			if (mode & 1 << 4) {
				//dive/climb
			}
		}
	}
}
