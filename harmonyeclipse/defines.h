/*
 * defines.h
 *
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define LEDinit 	DDRD |= (1 << LED1);	DDRD |= (1 << LED2);
#define LED1  PD2
#define LED2  PD3
#define LED1on PORTD |= (1 << LED1);
#define LED1off PORTD &= ~(1 << LED1);
#define LED2on PORTD |= (1 << LED2);
#define LED2off PORTD &= ~(1 << LED2);

#define SIDInit	DDRA|= 0xff; DDRB|= 0x1f; DDRD|= 0xf0;
#define SIDAdress PORTB
#define SIDData PORTA
#define SIDStartOsc PORTD |= 1<<4;
#define SIDStopOsc PORTD &= ~(1<<4);
#define SIDSetCS PORTD |= 1<<5;
#define SIDUnsetCS PORTD &= ~(1<<5);
#define SIDSetRW PORTD |= 1<<6;
#define SIDUnsetRW PORTD &= ~(1<<6);
#define SIDSetReset PORTD |= 1<<7;
#define SIDUnsetReset PORTD &= ~(1<<7);
#define DACDDRInit DDRC|=0xff;
#define DACport PORTC

//by https://www.mikrocontroller.net/topic/259610#2687480
#define GET_FAR_ADDRESS(var)                          \
({                                                    \
    uint_farptr_t tmp;                                \
                                                      \
    __asm__ __volatile__(                             \
                                                      \
            "ldi    %A0, lo8(%1)"           "\n\t"    \
            "ldi    %B0, hi8(%1)"           "\n\t"    \
            "ldi    %C0, hh8(%1)"           "\n\t"    \
            "clr    %D0"                    "\n\t"    \
        :                                             \
            "=d" (tmp)                                \
        :                                             \
            "p"  (&(var))                             \
    );                                                \
    tmp;                                              \
})

#endif /* DEFINES_H_ */
