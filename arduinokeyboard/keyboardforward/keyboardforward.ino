#include "Keyboard.h"

//char keyboard[256];
int keyboardmode = A9;



//void clearkeyboard() {
//    for (int i=0; i<256; i++) {
//      keyboard[i]=0;
//  }
//}

void setup() {
  Serial1.begin(9600);
  Keyboard.begin();
}

void loop() {
  if (Serial1.available() > 0) {
    // read incoming serial data:
    int inChar = Serial1.read();
    if (inChar >127) {
      keyboardmode = inChar;
      if(inChar == 0xAE) {
//          clearkeyboard();
          Keyboard.releaseAll();
      }
    } else {
      switch (keyboardmode){
        case 0xAA:
          //press normal key
///          keyboard[inChar] = 3;
          Keyboard.press(inChar);
          break;
        case 0xAB:
          //release normal key
//          keyboard[inChar] = 2;
          Keyboard.release(inChar);
          break;
        case 0xAC:
          //press special key
//          keyboard[inChar+128] = 3;
          Keyboard.press(128+inChar);
          break;
        case 0xAD:
          //release special key
//          keyboard[inChar+128] = 2;
          Keyboard.release(128+inChar);
          break;
        default:
          //NOP
          break;
      }
    }
  }
}
