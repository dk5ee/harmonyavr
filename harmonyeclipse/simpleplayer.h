/*
 * simpleplayer.h
 *
 */

#ifndef SIMPLEPLAYER_H_
#define SIMPLEPLAYER_H_
#include <avr/io.h>
#include "sidfunctions.h"
#include "defines.h"
void initsteps();
void nextstep();

#endif /* SIMPLEPLAYER_H_ */
