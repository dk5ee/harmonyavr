/*
 * sidfunctions.h
 *
 */

#ifndef SIDFUNCTIONS_H_
#define SIDFUNCTIONS_H_
#include <avr/io.h>

uint8_t sidreadreg(uint8_t reg);

void sidwritereg(uint8_t reg, uint8_t value);

void sidwritemaskedreg(uint8_t reg, uint8_t value, uint8_t mask);

/*
 channel 0..2 normal voices
 channel 3: frequency cutoff
 */
void sidsetnote(uint8_t channel, uint8_t note);
void sidsetfreq(uint8_t channel, uint16_t value);
uint16_t sidgetfreq(uint8_t value);
void sidsetADSR(uint8_t channel, uint8_t AD, uint8_t SR);
void sidsetcontrol(uint8_t channel, uint8_t control);
void sidsetfilterfreq(uint16_t filter);
void sidsetRESFilt(uint8_t resfilt);
void sidsetmode(uint8_t mode);
void sidsetwaveform(uint8_t channel, uint8_t waveform);
void sidsetgate(uint8_t channel, uint8_t gate);
void sidsetring(uint8_t channel, uint8_t value);
void sidsetsync(uint8_t channel, uint8_t value);
void sidsettest(uint8_t channel, uint8_t value);
void sidsetA(uint8_t channel, uint8_t value);
void sidsetD(uint8_t channel, uint8_t value);
void sidsetS(uint8_t channel, uint8_t value);
void sidsetR(uint8_t channel, uint8_t value);
void sidsetres(uint8_t value);
void sidsetfilter(uint8_t value);
void sidsetvolume(uint8_t value);
void sidsetfiltertype(uint8_t value);
void sidsetdisable3(uint8_t value);
void sidloadinstrument(uint8_t channel, uint8_t instrument);
void sidinit();
#endif /* SIDFUNCTIONS_H_ */
