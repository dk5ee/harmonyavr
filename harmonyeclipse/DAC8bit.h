/*
 * DAC8bit.h
 *
 */

#ifndef DAC8BIT_H_
#define DAC8BIT_H_
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
void dac_setfreq(uint16_t freq);
void dac_setmulti(uint8_t multi);
void dac_init();
void dac_playsample(uint8_t wavenum);
void dac_loadwave(uint8_t wave, uint8_t wavtablenum);
uint16_t dac_getintcount();

#endif /* DAC8BIT_H_ */
