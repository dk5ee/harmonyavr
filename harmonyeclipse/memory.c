/*
 * memory.c
 *
 */

#include "memory.h"
#include "sidfunctions.h"
#include <avr/pgmspace.h>
uint8_t readfakememory(uint16_t address) {
	if (address < 256) {
		return c64zeropage_6502[address];
	}
	if (address < 512) {
		return c64stack_6502[address - 256];
	}
	if (address >= 0xFFF0) {
		return c64top_6502[address - 0xFFF0];
	}
	if (address >= 0xD400 && address <= 0xD7FF) {
		uint8_t reg = address & 0x1f;
		return (sidreadreg(reg));
	}
	for (uint8_t mempage = 0; mempage < 3; mempage++) {
		uint16_t mempos = wavetablepos[mempage];
		if (address > mempos && address <= mempos + SINGLESAMPLESIZE) {
			return wavetableram[mempos][address - mempos];
		}
	}
	return 0; //default;
}
void writefakememory(uint16_t address, uint8_t data) {
	if (address < 256) {
		c64zeropage_6502[address] = data;
	}
	if (address < 512) {
		c64stack_6502[address - 256] = data;
	}
	if (address >= 0xFFF0) {
		c64top_6502[address - 0xFFF0] = data;
	}
	if (address >= 0xD400 && address <= 0xD7FF) {
		uint8_t reg = address & 0x1f;
		sidwritereg(reg, data);
	}
	for (uint8_t mempage = 0; mempage < 3; mempage++) {
		uint16_t mempos = wavetablepos[mempage];
		if (address > mempos && address <= mempos + SINGLESAMPLESIZE) {
			wavetableram[mempos][address - mempos] = data;
		}
	}

}
uint16_t loadROMtoRAMsize(uint8_t ramnumber, long rompos, uint16_t targetpos,
		uint16_t length) {
	if (ramnumber >= RAMWAVETABLES) return 0;
	wavetablepos[ramnumber]=targetpos;
	long pos = rompos;
	uint16_t i=0;
	for (i=0; (i< SINGLESAMPLESIZE)&&(i< length);i++) {
		uint8_t data = pgm_read_byte_far(pos);
		wavetableram[ramnumber][i] = data;
	}
	wavetablesize[ramnumber]=i;
	wavetablenext[ramnumber]=RAMWAVETABLES;
	return i;
}
void loadROMtoRAMmax(long rompos, uint16_t targetpos, uint16_t length) {
	long pos = rompos;
	uint16_t target = targetpos;

	for (uint8_t i=0; i< RAMWAVETABLES && length>=0;i++) {
		uint16_t loaded = loadROMtoRAMsize(i,pos,target,length);
		if (loaded >= length) {length =0;} else {length-=loaded;}
		pos += loaded;
		target+=loaded;

	}
}
