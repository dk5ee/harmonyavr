/*
 * ringuart0.c
 */
#include "ringuart0.h"
#include "defines.h"

//bitsize should be between 2 and 7
#define RINGUART0_RX0_BUFFER_BITSIZE 7
#define RINGUART0_TX0_BUFFER_BITSIZE 7
#define RX0RING_BUFFER (1<<RINGUART0_RX0_BUFFER_BITSIZE)
#define RX0RING_WRAP (RX0RING_BUFFER -1 )
#define TX0RING_BUFFER (1<<RINGUART0_TX0_BUFFER_BITSIZE)
#define TX0RING_WRAP (TX0RING_BUFFER -1 )

#define my0U2X U2X0
#define my0UBRRL UBRR0L
#define my0UBRRH UBRR0H
#define my0UCSRA UCSR0A
#define my0UCSRB UCSR0B
#define my0UCSRC UCSR0C
#define my0UCSZ0 UCSZ00
#define my0UCSZ1 UCSZ01
#define my0UDR UDR0
#define my0UDRIE UDRIE0
#define my0RX0CIE RXCIE0
#define my0RX0EN RXEN0
#define my0TX0CIE TXCIE0
#define my0TX0EN TXEN0

static uint8_t Rx0Ring[RX0RING_BUFFER];
static uint8_t Tx0Ring[TX0RING_BUFFER];
static volatile uint8_t Rx0Head = 0;
static volatile uint8_t Rx0Tail = 0;
static volatile uint8_t Rx0Status = 0;
static volatile uint8_t Tx0Head = 0;
static volatile uint8_t Tx0Tail = 0;

static void uart0_31250(void) {
#if defined BAUD
#undef BAUD
#endif
#define BAUD 9600
#include <util/setbaud.h>
	my0UBRRL = UBRRL_VALUE;
	my0UBRRH = UBRRH_VALUE;
	my0UCSRB = ((1 << my0RX0CIE) | (1 << my0RX0EN) | (1 << my0TX0EN));
	my0UCSRC = (1 << my0UCSZ1) | (1 << my0UCSZ0);
#if USE_2X
	my0UCSRA |= (1 << my0U2X);
#else
	my0UCSRA &= ~(1 << my0U2X);
#endif
}

void ringuart0init() {
	cli();
	uart0_31250();
	sei();
}
void tx0ring_enqueue(uint8_t data) {
	//wait while tx0ring is full..
	while ((((Tx0Tail + 1) - Tx0Head) & TX0RING_WRAP) == 0) {
		//do nothing
		// TODO: idle function?
	}
	uint8_t tempadr = (Tx0Tail + 1) & TX0RING_WRAP;
	Tx0Ring[tempadr] = data;
	Tx0Tail = tempadr;
	//now: always activate interrupt for sending
	my0UCSRB |= (1 << my0UDRIE);
}
uint8_t tx0ring_count() {
	return ((Tx0Tail - Tx0Head) & TX0RING_WRAP);
}
uint8_t tx0ring_space() {
	return (TX0RING_WRAP - ((Tx0Tail - Tx0Head) & TX0RING_WRAP));
}
uint8_t rx0ring_count() {
	return ((Rx0Tail - Rx0Head) & RX0RING_WRAP);
}
uint8_t rx0ring_space() {
	return (RX0RING_WRAP - ((Rx0Tail - Rx0Head) & RX0RING_WRAP));
}
uint8_t rx0ring_dequeue() {
	if (Rx0Tail == Rx0Head) {
		return 255;
	} else {
		uint8_t tempadr = (Rx0Head + 1) & RX0RING_WRAP;
		Rx0Head = tempadr;
		return Rx0Ring[tempadr];
	}
}
uint8_t rx0ring_peek(uint8_t offset) {
	return Rx0Ring[(1 + offset + Rx0Head) & RX0RING_WRAP];
}

/*void ring_outbyte(uint8_t data) {
 tx0ring_enqueue('0');
 tx0ring_enqueue('x');
 tx0ring_enqueue(hex[(data >> 4)]);
 tx0ring_enqueue(hex[data & 15]);
 }*/
inline void ring_outchar(uint8_t data) {
	//wrapper
	tx0ring_enqueue(data);
}

ISR ( USART0_RX_vect ) {
	LED2on
	uint8_t data;
	data = my0UDR;
	uint8_t temp;
	temp = ((Rx0Tail + 1) & RX0RING_WRAP);
	if (temp == Rx0Head) {
		Rx0Status |= 4; //overflow happened.. OMG!
	} else {
		Rx0Tail = temp;
		Rx0Ring[temp] = data;
	}
	LED2off
}
ISR(USART0_UDRE_vect) {
	if ((Tx0Tail - Tx0Head) & TX0RING_WRAP) {
		uint8_t temp;
		temp = ((Tx0Head + 1) & TX0RING_WRAP);
		Tx0Head = temp;
		my0UDR = Tx0Ring[temp];
	} else {
		my0UCSRB &= ~(1 << my0UDRIE); //disable sending interrupt
	}
}

// unused
EMPTY_INTERRUPT(USART0_TX_vect);

