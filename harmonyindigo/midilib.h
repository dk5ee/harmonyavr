/*
 * midilib.h
 *
 */

#ifndef MIDILIB_H_
#define MIDILIB_H_
#include "ringuart.h"
#include "defines.h"

void midiinparse();
void midiinit();

#endif /* MIDILIB_H_ */
