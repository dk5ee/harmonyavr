/*
 * c64keyboard.c
 *
 */

#include "c64keyboard.h"
#include <avr/io.h>
#include <util/delay.h>
#include "ringuart1.h"
uint8_t keyarray[8];
uint8_t oldkeyarray[8];
uint8_t keyrestore;
uint8_t oldkeyrestore;
uint8_t specialkeys; // bit0: CTRL bit1: C= bit2: shiftl bit3: shiftr

void printkey(uint8_t key) {
	tx1ring_enqueue(0xAA);
	tx1ring_enqueue(key);
	tx1ring_enqueue(0xAB);
	tx1ring_enqueue(key);
	tx1ring_enqueue(0xA9);
}
void printskey(uint8_t key) {
	tx1ring_enqueue(0xAC);
	tx1ring_enqueue(key);
	tx1ring_enqueue(0xAD);
	tx1ring_enqueue(key);
	tx1ring_enqueue(0xA9);
}

void describekey(uint8_t key) {
	printkey('0' + (key >> 6));
	printkey('0' + ((key >> 3) & 7));
	printkey('0' + (key & 7));
	printskey(KEY_RETURN);
}

void dodebugoutput(uint8_t specialkey) {
	for (uint8_t i = 32; i < 128; i++) {
		if (specialkey)
			sendSkey(specialkey, 1);
		printkey(i);
		if (specialkey)
			sendSkey(specialkey, 0);
		printkey('x');
		describekey(i);
	}
}

void sendnormalkey(uint8_t key, uint8_t down) {
	if (specialkeys & 0x01) {
		senddownSkey(KEY_LEFT_CTRL);
	}
	if (specialkeys & 0x02) {
		senddownSkey(KEY_RIGHT_ALT);
	}
	if (specialkeys & 0x0C) {
		senddownSkey(KEY_LEFT_SHIFT);
	}
	sendkey(key, down);
	if (specialkeys & 0x01) {
		sendupSkey(KEY_RIGHT_CTRL);
	}
	if (specialkeys & 0x02) {
		sendupSkey(KEY_RIGHT_ALT);
	}
	if (specialkeys & 0x0C) {
		sendupSkey(KEY_LEFT_SHIFT);
	}
}

void processkey(uint8_t key, uint8_t down) {
	switch (key) {
	case 0x00:
		sendnormalkey('1', down);
		break;
	case 0x01:
		//todo: left arrow
		break;
	case 0x02:
		//CTRL
		if (down) {
			specialkeys |= 1 << 0;
		} else {
			specialkeys &= ~(1 << 0);
		}
		break;
	case 0x03:
		//runstop: excape
		sendSkey(KEY_ESC, down);
		break;
	case 0x04:
		// SPACE
		sendkey(' ', down);
		break;
	case 0x05:
		//COMMODORE
		if (down) {
			specialkeys |= 1 << 1;
		} else {
			specialkeys &= ~(1 << 1);
		}
		break;
	case 0x06:
		sendnormalkey('q', down);
		break;
	case 0x07:

		sendnormalkey('2', down);
		break;
	case 0x08:
		//todo: # when big
		sendnormalkey('3', down);
		break;
	case 0x09:
		sendnormalkey('w', down);
		break;
	case 0x0A:
		sendnormalkey('a', down);
		break;
	case 0x0B:
		//shiftLEFT
		if (down) {
			specialkeys |= 1 << 2;
		} else {
			specialkeys &= ~(1 << 2);
		}
		break;
	case 0x0C:
		sendnormalkey('y', down);
		break;
	case 0x0D:
		sendnormalkey('s', down);
		break;
	case 0x0E:
		sendnormalkey('e', down);
		break;
	case 0x0F:
		sendnormalkey('4', down);
		break;
	case 0x10:
		sendnormalkey('5', down);
		break;
	case 0x11:
		sendnormalkey('r', down);
		break;
	case 0x12:
		sendnormalkey('d', down);
		break;
	case 0x13:
		sendnormalkey('x', down);
		break;
	case 0x14:
		sendnormalkey('c', down);
		break;
	case 0x15:
		sendnormalkey('f', down);
		break;
	case 0x16:
		sendnormalkey('t', down);
		break;
	case 0x17:
		sendnormalkey('6', down);
		break;
	case 0x18:
		//todo: apostroph when shif
		sendnormalkey('7', down);
		break;
	case 0x19:
		sendnormalkey('z', down);
		break;
	case 0x1A:
		sendnormalkey('g', down);
		break;
	case 0x1B:
		sendnormalkey('v', down);
		break;
	case 0x1C:
		sendnormalkey('b', down);
		break;
	case 0x1D:
		sendnormalkey('h', down);
		break;
	case 0x1E:
		sendnormalkey('u', down);
		break;
	case 0x1F:
		sendnormalkey('8', down);
		break;
	case 0x20:
		sendnormalkey('9', down);
		break;
	case 0x21:
		sendnormalkey('i', down);
		break;
	case 0x22:
		sendnormalkey('j', down);
		break;
	case 0x23:
		sendnormalkey('n', down);
		break;
	case 0x24:
		sendnormalkey('m', down);
		break;
	case 0x25:
		sendnormalkey('k', down);
		break;
	case 0x26:
		sendnormalkey('o', down);
		break;
	case 0x27:
		//todo
		sendkey('+', down);
		break;
	case 0x28:
		//todo
		sendkey('-', down);
		break;
	case 0x29:
		sendnormalkey('p', down);
		break;
	case 0x2A:
		sendnormalkey('l', down);
		break;
	case 0x2B:
		//todo
		sendkey(',', down);
		break;
	case 0x2C:
		//todo
		sendkey('.', down);
		break;
	case 0x2D:
		//todo
		// '('
		break;
	case 0x2E:
		//todo
		// "@"
		break;
	case 0x2F:
		// ","
		break;
	case 0x30:
		//todo
		// pound sign
		break;
	case 0x31:
		//todo
		sendkey('*', down);
		break;
	case 0x32:
		//todo
		// ')'
		break;
	case 0x33:
		//todo
		// '?' and '/'
		sendkey('/', down);
		break;
	case 0x34:
		//shiftr
		if (down) {
			specialkeys |= 1 << 3;
		} else {
			specialkeys &= ~(1 << 3);
		}
		break;
	case 0x35:
		//todo
		// '='
		break;
	case 0x36:
		//todo
		//UP arrow
		break;
	case 0x37:
		//todo
		//HOME
		break;
	case 0x38:
		//DELETE
		//todo
		sendSkey(KEY_BACKSPACE, down);
		break;
	case 0x39:
		// ENTER
		//todo
		sendSkey(KEY_RETURN, down);
		break;
	case 0x3A:
		//todo
		//leftright
		break;
	case 0x3B:
		//todo
		//updown
		break;
	case 0x3C:
		//F1
		//todo
		if (down)
			dodebugoutput(0);
		break;
	case 0x3D:
		//F3
		//todo
		if (down) {
			dodebugoutput(KEY_LEFT_SHIFT);
		}
		break;
	case 0x3E:
		//F5
		//todo
		if (down) {
			dodebugoutput(KEY_RIGHT_ALT);
		}
		break;
	case 0x3F:
		//F7
		//todo
		break;
	case 0x40:
#
		break;
	default:
		break;
	}
}

void setcolup(uint8_t col) {
	if (col != 0) {
		PORTB |= 1 << PB4;
	} else {
		PORTB &= ~(1 << PB4);
	}
	if (col != 1) {
		PORTB |= 1 << PB3;
	} else {
		PORTB &= ~(1 << PB3);
	}
	if (col != 2) {
		PORTB |= 1 << PB2;
	} else {
		PORTB &= ~(1 << PB2);
	}
	if (col != 3) {
		PORTB |= 1 << PB1;
	} else {
		PORTB &= ~(1 << PB1);
	}
	if (col != 4) {
		PORTB |= 1 << PB0;
	} else {
		PORTB &= ~(1 << PB0);
	}
	if (col != 5) {
		PORTD |= 1 << PD5;
	} else {
		PORTD &= ~(1 << PD5);
	}
	if (col != 6) {
		PORTD |= 1 << PD6;
	} else {
		PORTD &= ~(1 << PD6);
	}
	if (col != 7) {
		PORTD |= 1 << PD7;
	} else {
		PORTD &= ~(1 << PD7);
	}
}

void init64ports() {
	DDRD |= (1 << PD5) | (1 << PD6) | (1 << PD7);
	DDRB |= (1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3) | (1 << PB4);
	PORTD |= (1 << PD4); //set internal pullup for PD4
	PORTA |= 255; //set internal pullups for PORTA
}
uint8_t getoldkey(uint8_t key) {
	if (key == 64)
		return oldkeyrestore;
	if (key < 64) {
		uint8_t col = key >> 3;
		uint8_t row = key & 0x07;
		uint8_t value = oldkeyarray[col] & (1 << row);
		if (value)
			return 1;
	}
	return 0;
}
uint8_t getkey(uint8_t key) {
	if (key == 64)
		return keyrestore;
	if (key < 64) {
		uint8_t col = key >> 3;
		uint8_t row = key & 0x07;
		uint8_t value = keyarray[col] & (1 << row);
		if (value)
			return 1;
	}
	return 0;
}

void scankeys() {
	for (uint8_t i = 0; i < 8; i++) {
		setcolup(i);
		_delay_us(20);
		oldkeyarray[i] = keyarray[i];
		keyarray[i] = ~PINA;
	}
	//stupid debounce..
	for (uint8_t debounce = 0; debounce < 4; debounce++) {
		for (uint8_t i = 0; i < 8; i++) {
			setcolup(i);

			_delay_us(20);
			keyarray[i] |= ~PINA;
		}
	}
	oldkeyrestore = keyrestore;
	if ((PORTD & (1 << PD4)) == 0) {
		keyrestore = 1;
	} else {
		keyrestore = 0;
	}
	oldkeyrestore = keyrestore;
	for (uint8_t i = 0; i < 65; i++) {
		uint8_t then = getoldkey(i);
		uint8_t now = getkey(i);
		if (then != now) {
			processkey(i, now);
		}
	}
	setcolup(255); //disable..
}

