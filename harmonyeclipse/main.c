/*
 * main.c
 *
 */

#include <avr/io.h>
#include <util/delay.h>
#include "defines.h"
#include "midilib.h"
#include "sidfunctions.h"
#include "DAC8bit.h"
#include "effects.h"
#include "simpleplayer.h"
#include <avr/pgmspace.h>
#include "cpu.h"

const char text0[] PROGMEM = "https://gitlab.com/dk5ee/harmony 2020";

uint8_t filtermode = 1;

uint8_t filterres = 1;
uint16_t filterfreqs[] = { 1, 63, 255, 1023, 2047 };
uint8_t thisffreq = 0;

void filter() {
	sidsetfilterres(filterres - 1);
	filterres *= 2;

	if (filterres > 16) {
		filterres = 1;
		thisffreq++;
		if (thisffreq > 4) {
			thisffreq = 0;
			filtermode += 1;
			if (filtermode > 7) {
				filtermode = 1;
			}
			sidsetfilterpass(filtermode);

		}
		sidsetfilterfreq(filterfreqs[thisffreq]);
	}
}

int main(void) {
	LED1on
	LED2off
	midiinit();
	sidinit();

	LEDinit

	//sidsetvolume(15);
	//sidsetnote(0, 60);
	//sidsetnote(1, 60);

	_delay_ms(125);

	dac_init();

	dac_playsample(0);
	sidsetenablefilter(13);
	sei();
	uint16_t freqss[] = { 440, 880, 660, 770, 550, 440, 660, 0 };
	uint8_t freqsa = 8;
	uint8_t freqsc = 0;
	while (1) {

		dac_setmulti((1 + freqsc) * 16);

		LED2on
		LED1off

		_delay_ms(100);

		sidsetnote(0, 60 + (freqsc & 1));
		sidsetnote(2, 30 + (freqsc & 1));
		sidsetgate(0, 1);
		sidsetgate(1, 0);
		sidsetgate(2, 0);

		LED1on
		LED2off

		_delay_ms(100);
		dac_setfreq(freqss[freqsc]);
		freqsc++;
		if (freqsc >= freqsa) {
			freqsc = 0;
			filter();
			sidsetgate(1, 1);
		} else {
			sidsetgate(0, 0);

			sidsetgate(2, 1);
		}

	}
	return 0;
}
