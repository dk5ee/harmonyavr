/*
 * ringuart1.h
 * 9600 baud uart for com with usb hid
 *
 */

#ifndef RINGUART1_H_
#define RINGUART1_H_
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/twi.h>
#include <avr/pgmspace.h>


#define KEY_LEFT_CTRL (0x80	 & 0x7f)
#define KEY_LEFT_SHIFT (0x81 & 0x7f)
#define KEY_LEFT_ALT (0x82 & 0x7f)
#define KEY_LEFT_GUI (0x83 & 0x7f)
#define KEY_RIGHT_CTRL (0x84 & 0x7f)
#define KEY_RIGHT_SHIFT (0x85 & 0x7f)
#define KEY_RIGHT_ALT (0x86 & 0x7f)
#define KEY_RIGHT_GUI (0x87 & 0x7f)
#define KEY_UP_ARROW (0xDA & 0x7f)
#define KEY_DOWN_ARROW (0xD9 & 0x7f)
#define KEY_LEFT_ARROW (0xD8 & 0x7f)
#define KEY_RIGHT_ARROW (0xD7 & 0x7f)
#define KEY_BACKSPACE (0xB2 & 0x7f)
#define KEY_TAB (0xB3 & 0x7f)
#define KEY_RETURN (0xB0 & 0x7f)
#define KEY_ESC (0xB1 & 0x7f)
#define KEY_INSERT (0xD1 & 0x7f)
#define KEY_DELETE (0xD4 & 0x7f)
#define KEY_PAGE_UP (0xD3 & 0x7f)
#define KEY_PAGE_DOWN (0xD6 & 0x7f)
#define KEY_HOME (0xD2 & 0x7f)
#define KEY_END (0xD5 & 0x7f)
#define KEY_CAPS_LOCK (0xC1 & 0x7f)
#define KEY_F1 (0xC2 & 0x7f)
#define KEY_F2 (0xC3 & 0x7f)
#define KEY_F3 (0xC4 & 0x7f)
#define KEY_F4 (0xC5 & 0x7f)
#define KEY_F5 (0xC6 & 0x7f)
#define KEY_F6 (0xC7 & 0x7f)
#define KEY_F7 (0xC8 & 0x7f)
#define KEY_F8 (0xC9 & 0x7f)
#define KEY_F9 (0xCA & 0x7f)
#define KEY_F10 (0xCB & 0x7f)

void senddownkey(uint8_t key);
void sendupkey(uint8_t key);
void senddownSkey(uint8_t key);
void sendupSkey(uint8_t key);
void sendkey(uint8_t key, uint8_t down);
void sendSkey(uint8_t key, uint8_t down);
void ringuart1init();
void tx1ring_enqueue(uint8_t data); // - am ende einfügen. automatisch senden starten.
uint8_t tx1ring_count(); // - noch zu sendende zeichen im buffer
uint8_t tx1ring_space(); // - noch verfügbarer space im buffer
uint8_t rx1ring_count(); // - empfangene zeichen im buffer
uint8_t rx1ring_space(); // - noch verfügbarer space im buffer
uint8_t rx1ring_dequeue(); // - erstes zeichen entfernen und ausliefern
uint8_t rx1ring_peek(uint8_t offset); // - kommendes zeichen entfernen
uint8_t rx1status();
uint8_t rx1resetstatus();

#endif /* RINGUART1_H_ */
