/*
 * midilib.c
 *
 */
#include "midilib.h"

uint8_t midirunningmode = 0;
uint8_t mididatacount = 0;
uint8_t mididataamount = 0;
uint8_t midichannel = 0;
uint8_t midivelocity = 0;
uint8_t mididata1;
uint8_t mididata2;
uint16_t bigvalue = 0;

void midiinparse() {
	while (rxring_count()) {
		uint8_t ch = rxring_dequeue();
		if (ch & (1 << 7)) {
			uint8_t channel = ch & 0x0f;
			switch (ch >> 4) {
			case 8:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 2;
				midichannel = channel;
				//note off

				break;
			case 9:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 2;
				midichannel = channel;
				//note on
				LED1on
				break;
			case 10:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 2;
				midichannel = channel;
				//aftertouch
				break;
			case 11:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 2;
				midichannel = channel;
				//control change
				break;
			case 12:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 1;
				midichannel = channel;
				//programm change change
				break;
			case 13:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 1;
				midichannel = channel;
				//channel pressure
				break;
			case 14:
				midirunningmode = ch;
				mididatacount = 0;
				mididataamount = 2;
				midichannel = channel;
				//Pitch Bend Change
				break;
			case 15:
				if (ch >= 0xf0 && ch <= 0xf7) {
					midirunningmode = 0; // stop running mode, else ignore
				} else {
					//System Real-Time Messages
					//todo: check running mode in specs
				}
				mididatacount = 0;
				mididataamount = 0;
				//system exclusive
				break;
			default:
				//should not happen
				break;
			}
			//endcontrol
		} else {

			if (mididataamount == 0) {
				// do nothing
			} else if (mididataamount == 1) {

			} else if (mididataamount == 2) {
				if (mididatacount == 0) {
					mididata1 = ch;
					mididatacount++;
				} else {
					mididata2 = ch;
					mididatacount = 0;
					switch (midirunningmode >> 4) {
					case 8:
						LED1off
						break;
					case 9:
						if (mididata2 == 0) {
							//velocity zero => note off
							LED1off
						} else {
							LED1on
						}
						break;
					default:
						break;
					}
				}
			} else {

			}
			//end data

		}
	}
}

void midiinit() {
	midirunningmode = 0;
	ringuartinit();
}

